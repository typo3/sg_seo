<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgSeo\Events;

/**
 * Event, which allows to rule out pages based on own rules.
 */
class ShouldIncludePageInSitemapEvent {
	/**
	 * Event must be true by default.
	 *
	 * @var bool
	 */
	protected $pageValid = TRUE;

	/**
	 * @var array
	 */
	protected $pageInfo = [];

	/**
	 * @var int
	 */
	protected $language = 0;

	public function __construct(array $pageInfo, int $language) {
		$this->pageInfo = $pageInfo;
		$this->language = $language;
	}

	/**
	 * @return bool
	 */
	public function getPageValid(): bool {
		return $this->pageValid;
	}

	/**
	 * @param bool $pageValid
	 */
	public function setPageValid(bool $pageValid): void {
		$this->pageValid = $pageValid;
	}

	/**
	 * @return array
	 */
	public function getPageInfo(): array {
		return $this->pageInfo;
	}

	/**
	 * @return int
	 */
	public function getLanguage(): int {
		return $this->language;
	}
}
