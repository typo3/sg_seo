<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgSeo\Events;

use SGalinski\SgSeo\Renderers\AbstractSitemapRenderer;

/**
 * Small event to override current Sitemap Renderer
 */
class OverrideSitemapRendererEvent {
	/** @var AbstractSitemapRenderer */
	protected $renderer;

	/**
	 * Set the renderer to your own renderer, does not allow NULL Values.
	 * Must be an Instance of AbstractSitemapRenderer
	 *
	 * @param AbstractSitemapRenderer $renderer
	 * @return void
	 */
	public function setRenderer(AbstractSitemapRenderer $renderer): void {
		$this->renderer = $renderer;
	}

	/**
	 * Returns the Renderer or NULL in default.
	 *
	 * @return AbstractSitemapRenderer|null
	 */
	public function getRenderer(): ?AbstractSitemapRenderer {
		return $this->renderer;
	}
}
