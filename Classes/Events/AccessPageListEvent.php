<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgSeo\Events;

use TYPO3\CMS\Core\Site\Entity\Site;

/**
 * Class AccessPageListEvent
 *
 * @package SGalinski\SgSeo\Events
 */
class AccessPageListEvent {
	/**
	 * @var array
	 */
	protected $pageList = [];

	/**
	 * @var int
	 */
	protected $sysLanguageUid = 0;

	/**
	 * @var Site
	 */
	protected $site;

	/**
	 * @var array
	 */
	private $typoScriptConfiguration;

	/**
	 * AccessPageListEvent constructor.
	 *
	 * @param array $pageList
	 * @param int $sysLanguageUid
	 * @param Site $site
	 * @param array $typoScriptConfiguration
	 */
	public function __construct(array $pageList, int $sysLanguageUid, Site $site, array $typoScriptConfiguration = []) {
		$this->pageList = $pageList;
		$this->sysLanguageUid = $sysLanguageUid;
		$this->site = $site;
		$this->typoScriptConfiguration = $typoScriptConfiguration;
	}

	/**
	 * @return array
	 */
	public function getPageList(): array {
		return $this->pageList;
	}

	/**
	 * @return int
	 */
	public function getSysLanguageUid(): int {
		return $this->sysLanguageUid;
	}

	/**
	 * @param array $pageList
	 */
	public function setPageList(array $pageList): void {
		$this->pageList = $pageList;
	}

	/**
	 * Returns the Site from Event creating Sitemap Generator
	 *
	 * @return Site
	 */
	public function getSite(): Site {
		return $this->site;
	}

	/**
	 * Gets the typo script configuration
	 *
	 * @return array
	 */
	public function getTypoScriptConfiguration(): array {
		return $this->typoScriptConfiguration;
	}
}
