<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgSeo\Service;

use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Domain\Repository\PageRepository;
use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\Utility\CanonicalizationUtility;

/**
 * Service to help render the canonical/hreflang URL
 */
class UrlGenerationService implements SingletonInterface {
	protected ServerRequestInterface $request;
	protected array $page = [];
	protected PageRepository $pageRepository;
	protected ContentObjectRenderer $contentObjectRenderer;
	protected SiteFinder $siteFinder;
	public function __construct(
		PageRepository $pageRepository,
		SiteFinder $siteFinder
	) {
		$this->pageRepository = $pageRepository;
		$this->contentObjectRenderer = GeneralUtility::makeInstance(ContentObjectRenderer::class);
		$this->siteFinder = $siteFinder;
	}

	/**
	 * Returns the generated URL based on the given TSFE object and given language
	 *
	 * @param ServerRequestInterface $request
	 * @param int $languageId
	 * @param bool $allowQueryStringUsage
	 * @param array|null $page optional page record
	 * @return string
	 * @throws SiteNotFoundException
	 */
	public function generateUrlByCurrentRequest(
		ServerRequestInterface $request,
		int $languageId,
		bool $allowQueryStringUsage = FALSE,
		array $page = NULL
	): string {
		$canonical = '';
		$this->request = $request;
		$this->page = $this->request->getAttribute('frontend.controller')->page ?? [];
		if (is_array($page)) {
			$this->page = $page;
		}

		// Hint: Multilingual issues needs to be solved in the calling code.
		if (isset($this->page['canonicalbyextension']) && $this->page['canonicalbyextension']) {
			$canonical = $this->page['canonicalbyextension'];
		}

		// content from other page is shown
		if ($canonical === '') {
			$canonical = $this->checkContentFromPid();
		}

		// content from canonical
		if ($canonical === '') {
			$canonical = $this->checkForCanonicalLink($languageId);
		}

		// default canonical implementation with/without a query string
		// Note: TYPO3 would use the implementation with the query string based on the chash. Unfortunately this
		// is not configured by most people. So let's allow to define this behaviour via a checkbox.
		// https://docs.typo3.org/m/typo3/reference-coreapi/main/en-us/ApiOverview/GlobalValues/Typo3ConfVars/FE.html#globals-typo3-conf-vars-fe-cachehash
		if ($canonical === '') {
			$ignoreQueryString = TRUE;
			if (isset($GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['sg_seo']['requireQueryStringList'])) {
				$requireQueryStringList = GeneralUtility::trimExplode(
					',',
					$GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['sg_seo']['requireQueryStringList']
				);
				foreach ($requireQueryStringList as $key) {
					if (isset($_GET[$key])) {
						$ignoreQueryString = FALSE;
						break;
					}
				}
			}

			if ($ignoreQueryString) {
				$canonical = $this->generateUrl($languageId);
			} elseif ($allowQueryStringUsage) {
				$canonical = $this->generateUrlWithQueryStringParameters($languageId);
			}
		}

		return $canonical;
	}

	/**
	 * Taken from TYPO3\CMS\Seo\Canonical\CanonicalGenerator
	 *
	 * @param int $languageId
	 * @return string
	 */
	protected function checkForCanonicalLink(int $languageId): string {
		if (!empty($this->page['canonical_link'])) {
			return $this->contentObjectRenderer->typoLink_URL([
				'parameter' => $this->page['canonical_link'],
				'language' => $languageId,
				'forceAbsoluteUrl' => TRUE,
			]);
		}

		return '';
	}

	/**
	 * Taken from TYPO3\CMS\Seo\Canonical\CanonicalGenerator
	 *
	 * @return string
	 */
	protected function checkContentFromPid(): string {
		if (empty($this->page['content_from_pid'])) {
			return '';
		}

		$parameter = (int) $this->page['content_from_pid'];
		if ($parameter <= 0) {
			return '';
		}

		$targetPage = $this->pageRepository->getPage($parameter, TRUE);
		if (!empty($targetPage['canonical_link'])) {
			$parameter = $targetPage['canonical_link'];
		}

		return $this->contentObjectRenderer->typoLink_URL([
			'parameter' => $parameter,
			'forceAbsoluteUrl' => TRUE,
		]);
	}

	/**
	 * Generate a URL with the given language
	 *
	 * Note: If you render a page to an existing, disabled page overlay of the very same page, all generated links
	 * are rendered to links to the disabled ones instead of the correct URLs. This is a pretty strange TYPO3 bug that
	 * I could not solve here. In most cases this should not be an issue. The language menu doesn't have this problem
	 * by the way, but the link rendering is totally different there.
	 *
	 * @param int $language
	 * @return string
	 * @throws SiteNotFoundException
	 */
	protected function generateUrl(int $language): string {
		$params = [];
		if (isset($this->page['extensionArgumentsForCanonicalAndHrefLang'])) {
			parse_str($this->page['extensionArgumentsForCanonicalAndHrefLang'], $params);
		}

		$params['_language'] = $language;
		$site = $this->siteFinder->getSiteByPageId(
			$this->request->getAttribute('routing')->getPageId()
		);
		return $site->getRouter()->generateUri(
			!empty($this->page['content_from_pid']) ?
				$this->page['content_from_pid'] : $this->request->getAttribute('routing')->getPageId(),
			$params,
			'',
			$this->request->getAttribute('routing')->getPageType()
		);
	}

	/**
	 * Generate a URL with the given language including the query string
	 *
	 * @param int $language
	 * @return string
	 */
	public function generateUrlWithQueryStringParameters(int $language): string {
		return $this->contentObjectRenderer->typoLink_URL([
			'parameter' => $this->request->getAttribute('routing')->getPageId() . ',' . $this->request->getAttribute('routing')->getPageType(),
			'forceAbsoluteUrl' => TRUE,
			'addQueryString' => TRUE,
			'language' => $language,
			'addQueryString.' => [
				'method' => 'GET',
				'exclude' => implode(
					',',
					CanonicalizationUtility::getParamsToExcludeForCanonicalizedUrl(
						(int) $this->request->getAttribute('routing')->getPageId(),
						(array) $GLOBALS['TYPO3_CONF_VARS']['FE']['additionalCanonicalizedUrlParameters']
					)
				)
			]
		]);
	}
}
