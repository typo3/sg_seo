<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgSeo\Service;

use Closure;
use SGalinski\SgSeo\PageTitle\PageTitleProvider;
use TYPO3\CMS\Core\EventDispatcher\EventDispatcher;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\Http\RequestHandler;
use TYPO3\CMS\Seo\Canonical\CanonicalGenerator;

/**
 * We use this service when we want to regenerate the canonical and hreflang tags in uncached actions
 */
class HeadTagService implements SingletonInterface {
	/**
	 * @var string
	 */
	private $title;

	/**
	 * @var string
	 */
	private $description;

	/**
	 * @var string
	 */
	private $extensionArgumentsForCanonicalAndHrefLang;

	/**
	 * @var bool
	 */
	private $isCachable;

	/**
	 * @var bool
	 */
	private static $isInit = FALSE;

	/**
	 * @return bool
	 */
	public function isCachable(): bool {
		return $this->isCachable;
	}

	/**
	 * @param bool $isCachable
	 */
	public function setIsCachable(bool $isCachable): void {
		$this->isCachable = $isCachable;
	}

	/**
	 * @return string
	 */
	public function getTitle(): string {
		return $this->title;
	}

	/**
	 * @param string $description
	 */
	public function setDescription(string $description): void {
		$this->description = trim($description);
	}

	/**
	 * @return string
	 */
	public function getDescription(): string {
		return $this->description;
	}

	/**
	 * @param string $title
	 */
	public function setTitle(string $title): void {
		$this->title = trim($title);
	}

	/**
	 * @return string
	 */
	public function getExtensionArgumentsForCanonicalAndHrefLang(): string {
		return $this->extensionArgumentsForCanonicalAndHrefLang;
	}

	/**
	 * @param string $extensionArgumentsForCanonicalAndHrefLang
	 */
	public function setExtensionArgumentsForCanonicalAndHrefLang(
		string $extensionArgumentsForCanonicalAndHrefLang
	): void {
		$this->extensionArgumentsForCanonicalAndHrefLang = trim($extensionArgumentsForCanonicalAndHrefLang);
	}

	/**
	 * @param bool $isCachable
	 * @param string $title
	 * @param string $description
	 * @param string $extensionArgumentsForCanonicalAndHrefLang
	 */
	public function __construct(
		bool $isCachable,
		string $title,
		string $description,
		string $extensionArgumentsForCanonicalAndHrefLang
	) {
		$this->setIsCachable($isCachable);
		$this->setTitle($title);
		$this->setDescription($description);
		$this->setExtensionArgumentsForCanonicalAndHrefLang($extensionArgumentsForCanonicalAndHrefLang);
	}

	/**
	 * Re-generates the head tags: title, description, canonical and hrefLang
	 */
	public function execute(): void {
		if ($this->title !== '' && $this->title !== NULL) {
			$GLOBALS['TSFE']->page['titlebyextension'] = $this->title;
			$GLOBALS['TSFE']->page['og_title'] = $this->title;
			$GLOBALS['TSFE']->page['twitter_title'] = $this->title;
		}

		if ($this->description !== '') {
			$contentObject = GeneralUtility::makeInstance(ContentObjectRenderer::class);
			$description = trim(
				preg_replace(
					'!\s+!',
					' ',
					str_replace(
						["\r", "\n", "\r\n"],
						' ',
						$contentObject->cropHTML(strip_tags($this->description), '255|...|1')
					)
				)
			);

			if ($description !== '') {
				$GLOBALS['TSFE']->page['description'] = $description;
			}
		}

		if ($this->extensionArgumentsForCanonicalAndHrefLang !== '') {
			$GLOBALS['TSFE']->page['extensionArgumentsForCanonicalAndHrefLang'] =
				$this->extensionArgumentsForCanonicalAndHrefLang;
		}

		if ($this->isCachable) {
			return;
		}

		if ($this->description !== '') {
			$this->overwriteDescription();
		}

		if ($this->title !== '') {
			$this->overwriteTitle();
		}

		if ($this->extensionArgumentsForCanonicalAndHrefLang !== '') {
			$this->overwriteCanonicalAndHrefLang();
		}
	}

	/**
	 * Overwrites the meta description through the page renderer
	 */
	public function overwriteDescription(): void {
		$pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
		if ($GLOBALS['TSFE']->page['description']) {
			$pageRenderer->setMetaTag('name', 'description', $GLOBALS['TSFE']->page['description']);
		}
	}

	/**
	 * Regenerates the canonical tag
	 */
	public function overwriteCanonical(): void {
		// Generate Canonicals with the canonical generator
		$eventDispatcher = GeneralUtility::makeInstance(EventDispatcher::class);
		/** @var CanonicalGenerator $canonicalGenerator */
		$canonicalGenerator = GeneralUtility::makeInstance(
			CanonicalGenerator::class,
			$GLOBALS['TSFE'],
			$eventDispatcher
		);
		$canonicalGenerator->generate(['page' => $GLOBALS['TSFE']->page, 'request' => $GLOBALS['TYPO3_REQUEST']]);
	}

	/**
	 * Regenerates the Canonical and HrefLang tags
	 *
	 * NOTE: This function can only be called once. It doesn't make much sense that
	 * plugins overwrite each other here and waste important cycles.
	 */
	public function overwriteCanonicalAndHrefLang(): void {
		if (self::$isInit) {
			return;
		}

		$controller = $GLOBALS['TSFE'];
		// first clear any canonical and href parts that are available
		foreach ($controller->additionalHeaderData as $key => $headerData) {
			if (
				str_contains($headerData, '<link rel="alternate" hreflang="')
				|| str_contains($headerData, '<link rel="canonical"')
			) {
				unset($controller->additionalHeaderData[$key]);
			}
		}

		$this->overwriteCanonical();
		$this->overwriteHrefLang();
		self::$isInit = TRUE;
	}

	/**
	 * Regenerates the hrefLang tags
	 */
	public function overwriteHrefLang(): void {
		// Generate HrefLangs with the RequestHandler
		/** @var RequestHandler $requestHandler */
		$requestHandler = GeneralUtility::makeInstance(RequestHandler::class);
		// The closure is needed to grant access to a protected method
		$closure = function ($controller, $request) {
			$this->generateHrefLangTags($controller, $request);
		};
		$hrefLangGenerator = Closure::bind($closure, $requestHandler, $requestHandler);
		$hrefLangGenerator($GLOBALS['TSFE'], $GLOBALS['TYPO3_REQUEST']);
	}

	/**
	 * Overwrites the title in the title provider
	 */
	public function overwriteTitle(): void {
		$titleProvider = GeneralUtility::makeInstance(PageTitleProvider::class);
		$titleProvider->setTitle($GLOBALS['TSFE']->page['titlebyextension']);
		$pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
		if ($GLOBALS['TSFE']->page['og_title']) {
			$pageRenderer->setMetaTag('property', 'og:title', $GLOBALS['TSFE']->page['og_title']);
		}
		if ($GLOBALS['TSFE']->page['twitter_title']) {
			$pageRenderer->setMetaTag('property', 'twitter:title', $GLOBALS['TSFE']->page['twitter_title']);
		}
	}
}
