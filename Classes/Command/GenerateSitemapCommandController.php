<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgSeo\Command;

use Doctrine\DBAL\Driver\Exception;
use SGalinski\SgSeo\Generator\PagesSitemapGenerator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\Http\ImmediateResponseException;
use TYPO3\CMS\Core\Site\Entity\SiteLanguage;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\TypoScript\TemplateService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\RootlineUtility;

/**
 * Command controller for the sitemap generation
 */
class GenerateSitemapCommandController extends Command {
	/** @var array */
	private array $typoScriptConfiguration;

	/**
	 * The current index sitemaps
	 *
	 * @var array
	 */
	protected array $sitemaps = [];

	public function __construct(string $name = NULL) {
		parent::__construct($name);
		$this->typoScriptConfiguration = [];
	}

	/**
	 * Configure command
	 */
	public function configure(): void {
		$this->setDescription('Generate the sitemap XML files')
			->addArgument(
				'relativePathToSitemap',
				InputArgument::OPTIONAL,
				'The relative path to the sitemap files, leave empty for web root',
				''
			)
			->addArgument('siteRootId', InputArgument::OPTIONAL, 'The id of the root page to start the sitemap from', 1)
			->addArgument(
				'enableFilter',
				InputArgument::OPTIONAL,
				'Don\'t add hidden pages to the result (NOT "NOT_IN_MENU"!)',
				TRUE
			);
	}

	/**
	 * Execute the command
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @return int
	 * @throws Exception
	 * @throws ImmediateResponseException
	 * @throws SiteNotFoundException
	 */
	public function execute(InputInterface $input, OutputInterface $output): int {
		$relativePathToSitemap = $input->getArgument('relativePathToSitemap');
		if ($relativePathToSitemap !== '') {
			$relativePathToSitemap = '/' . ltrim((string) $relativePathToSitemap, '/');
		}

		$siteRootId = (int) $input->getArgument('siteRootId');
		$enableFilter = (bool) $input->getArgument('enableFilter');
		$siteFinder = GeneralUtility::makeInstance(SiteFinder::class);
		$site = $siteFinder->getSiteByPageId($siteRootId);
		$siteBaseUrl = rtrim((string) $site->getBase(), '/');
		$languages = $site->getAllLanguages();
		$defaultLanguage = $site->getDefaultLanguage();

		$typoScriptConfiguration = $this->getTypoScriptConfiguration($siteRootId);

		foreach ($typoScriptConfiguration['plugin']['tx_seo']['config'] as $sitemapName => $sitemapConfig) {
			$xslPath = PagesSitemapGenerator::getXslFilePath($typoScriptConfiguration, $sitemapName);
			$this->writeSitemaps(
				$sitemapName,
				$languages,
				$defaultLanguage,
				$relativePathToSitemap,
				$siteRootId,
				$enableFilter,
				$xslPath
			);
			$this->writeSitemapIndex($xslPath, $relativePathToSitemap, $sitemapName, $siteBaseUrl);
		}

		return self::SUCCESS;
	}

	/**
	 * Parses the typoscript config and gets the configuration for this site root
	 *
	 * @param int $siteRootId
	 * @return array
	 */
	protected function getTypoScriptConfiguration(int $siteRootId): array {
		if (!$this->typoScriptConfiguration) {
			// @todo: find a way to replace the TemplateService
			/** @var TemplateService $template */
			$template = GeneralUtility::makeInstance(TemplateService::class);
			// do not log time-performance information
			$template->tt_track = FALSE;
			// Explicitly trigger processing of extension static files
			$template->setProcessExtensionStatics(TRUE);
			// Get the root line
			$rootline = [];
			if ($siteRootId > 0) {
				// Get the rootline for the current page
				$rootline = GeneralUtility::makeInstance(
					RootlineUtility::class,
					$siteRootId
				)->get();
			}

			// This generates the constants/config + hierarchy info for the template.
			$template->runThroughTemplates($rootline);
			$template->generateConfig();
			$this->typoScriptConfiguration = GeneralUtility::removeDotsFromTS($template->setup);
		}

		return $this->typoScriptConfiguration;
	}

	/**
	 * Writes the sitemaps files (paginated) for the given SitemapName from the config
	 *
	 * @param string $sitemapName
	 * @param array $languages
	 * @param SiteLanguage $defaultLanguage
	 * @param string $relativePathToSitemap
	 * @param int $siteRootId
	 * @param bool $enableFilter
	 * @param string $xslPath
	 * @throws ImmediateResponseException
	 * @throws Exception
	 */
	protected function writeSitemaps(
		string $sitemapName,
		array $languages,
		SiteLanguage $defaultLanguage,
		string $relativePathToSitemap,
		int $siteRootId,
		$enableFilter,
		string $xslPath
	) {
		$sitemapFileNamePart = $this->getSitemapFilenameBySitemapName($sitemapName);
		$directoryToSitemap = Environment::getPublicPath() . $relativePathToSitemap;
		if (!is_dir($directoryToSitemap)) {
			GeneralUtility::mkdir_deep(Environment::getPublicPath() . $relativePathToSitemap);
		}

		// start writing language specific sitemaps
		foreach ($languages as $language) {
			if (!$language->enabled()) {
				continue;
			}

			$sitemapGenerator = GeneralUtility::makeInstance(
				PagesSitemapGenerator::class,
				$siteRootId,
				$language,
				$enableFilter,
				$xslPath
			);
			$sitemapGenerator->setTypoScriptConfiguration($this->getTypoScriptConfiguration($siteRootId));
			$sitemapEntryPages = array_chunk(
				$sitemapGenerator->getSitemapEntries($sitemapName),
				PagesSitemapGenerator::MAX_ENTRIES_PER_SITEMAP
			);

			foreach ($sitemapEntryPages as $pageIndex => $sitemapEntries) {
				$sitemapContent = $sitemapGenerator->returnSitemapContent($sitemapName, $sitemapEntries);
				$sitemapLanguageIdentifier = $language === $defaultLanguage ?
					'default' : $language->getFlagIdentifier();
				$fileName = $sitemapFileNamePart . '.' . str_replace('flags-', '', $sitemapLanguageIdentifier)
					. ($pageIndex > 0 ? '_' . (string) $pageIndex : '') . '.xml';
				$filePath = $directoryToSitemap . '/' . $fileName;
				$this->sitemaps[] = $fileName;
				GeneralUtility::writeFile($filePath, $sitemapContent, TRUE);
			}

			$sitemapGenerator->takeDownPageRepository();
		}
	}

	/**
	 * Writes the sitemap index file
	 *
	 * @param string $xslPath
	 * @param string $relativePathToSitemap
	 * @param string $sitemapName
	 * @param string $siteBaseUrl
	 */
	protected function writeSitemapIndex(
		string $xslPath,
		string $relativePathToSitemap,
		string $sitemapName,
		string $siteBaseUrl
	): void {
		// before we write XLS we have to check if it is an absolute path to current instance, else browsers stop
		// showing the sitemap.
		if (!str_contains($xslPath, $siteBaseUrl) && !str_contains($xslPath, 'http')) {
			$xslPath = $siteBaseUrl . $xslPath;
		}
		// start writing index sitemap
		// we hardcode http:// as protocol because of the Sitemap.xsl from TYPO3 who have it
		$indexContent = '<?xml version="1.0" encoding="UTF-8"?>'
			. '<?xml-stylesheet type="text/xsl" href="' . $xslPath . '"?>'
			. '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
		foreach ($this->sitemaps as $sitemapPath) {
			$indexContent .= '<sitemap><loc>' . $siteBaseUrl . rtrim($relativePathToSitemap, '/') .
				'/' . $sitemapPath . '</loc>' . '<lastmod>' . date('c') . '</lastmod></sitemap>';
		}

		$indexContent .= '</sitemapindex>';
		GeneralUtility::mkdir_deep(Environment::getPublicPath() . $relativePathToSitemap);
		$fileName = Environment::getPublicPath() .
			$relativePathToSitemap . '/' . $this->getSitemapFilenameBySitemapName($sitemapName) . '.xml';
		GeneralUtility::writeFile($fileName, $indexContent, TRUE);
		$this->sitemaps = [];
	}

	/**
	 * Get the sitemap filename by sitemap name.
	 * The default name is sitemap, and it corresponds to the configuration in the xmlSitemap config.
	 * This function makes sure to avoid conflicts with this name and reserve it.
	 *
	 * @param string $sitemapName
	 * @return string
	 */
	protected function getSitemapFilenameBySitemapName(string $sitemapName): string {
		if ($sitemapName === PagesSitemapGenerator::DEFAULT_SITEMAP_INDEX) {
			return 'sitemap';
		}

		return 'sitemap-' . strtolower($sitemapName);
	}
}
