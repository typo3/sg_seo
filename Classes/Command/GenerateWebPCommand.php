<?php

/*******************************************************************************
 * Copyright notice
 *
 * (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ******************************************************************************/

namespace SGalinski\SgSeo\Command;

use Doctrine\DBAL\Driver\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Registry;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Resource\ResourceStorage;
use TYPO3\CMS\Core\SysLog\Action as SystemLogGenericAction;
use TYPO3\CMS\Core\SysLog\Error as SystemLogErrorClassification;
use TYPO3\CMS\Core\SysLog\Type as SystemLogType;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Generates webp files from your processed files
 */
class GenerateWebPCommand extends Command {
	/**
	 * @var QueryBuilder
	 */
	protected $queryBuilder;

	/**
	 * @var Registry
	 */
	protected $registry;

	/**
	 * @var array
	 */
	protected $arguments = [];

	/**
	 * @var ResourceStorage
	 */
	protected $storage;

	/**
	 * Configure the command and set description and arguments
	 */
	public function configure(): void {
		$this->setDescription('Generate webp images from the processed images')
			->addArgument('storageUid', InputArgument::OPTIONAL, '(optional) The storage uid to process', 1)
			->addArgument('useLog', InputArgument::OPTIONAL, '(optional) Define if you want to use the log', 0)
			->addArgument(
				'savingPercentage',
				InputArgument::OPTIONAL,
				'(optional) Minimum percentage of filesize savings each image has to achieve in order to be used.',
				2
			)
			->addArgument(
				'imageQuality',
				InputArgument::OPTIONAL,
				'(optional) Define the image quality from 0 -100%.',
				80
			)
			->addArgument('batchSize', InputArgument::OPTIONAL, '(optional) The batch size.', 10000)
			->addArgument(
				'processGif',
				InputArgument::OPTIONAL,
				'(optional) Include gif images in the processing.',
				0
			);
	}

	/**
	 * Execute the command, converting images to webp
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @return int
	 * @throws Exception
	 */
	public function execute(InputInterface $input, OutputInterface $output): int {
		$this->arguments = $input->getArguments();
		$limit = $this->arguments['batchSize'] ?? 10000;

		/** @var ConnectionPool $connectionPool */
		$connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
		$this->registry = GeneralUtility::makeInstance(Registry::class);
		$start = $this->registry->get('sg_seo_webp', 'offset', 0);

		/** @var ResourceFactory $resourceFactory */
		$resourceFactory = GeneralUtility::makeInstance(ResourceFactory::class);
		$this->storage = $resourceFactory->getStorageObject($this->arguments['storageUid'] ?? 1);
		$this->queryBuilder = $connectionPool->getQueryBuilderForTable('sys_file_processedfile');
		$fileCount = $this->queryBuilder->count('*')
			->from('sys_file_processedfile')->where(
				$this->queryBuilder->expr()->eq(
					'storage',
					$this->queryBuilder->createNamedParameter(
						$this->arguments['storageUid'] ?? 1,
						Connection::PARAM_INT
					)
				)
			)->executeQuery()->fetchOne();

		$this->workBatch($start, $limit, $fileCount);
		return 0;
	}

	/**
	 * Work the batch until all files have been processed
	 *
	 * @param int $start
	 * @param int $limit
	 * @param int $fileCount
	 * @throws Exception
	 */
	protected function workBatch(int $start, int $limit, int $fileCount): void {
		$disallowGifCondition = '';
		if (!$this->arguments['processGif']) {
			$disallowGifCondition = $this->queryBuilder->expr()->notLike('name', '"%.gif"');
		}

		$files = $this->queryBuilder->select('*')
			->from('sys_file_processedfile')
			->where(
				$this->queryBuilder->expr()->and(
					$this->queryBuilder->expr()->eq(
						'storage',
						$this->queryBuilder->createNamedParameter(
							$this->arguments['storageUid'] ?? 1,
							Connection::PARAM_INT
						)
					),
					$this->queryBuilder->expr()->isNotNull('name'),
					$this->queryBuilder->expr()->notLike('name', '"%.webp"'),
					$disallowGifCondition
				)
			)
			->setFirstResult($start)->setMaxResults($limit)->executeQuery()->fetchAll();

		$imageQuality = $this->arguments['imageQuality'] ?? 80;
		$minimumSavingPercentage = $this->arguments['savingPercentage'] ?? 2;
		foreach ($files as $file) {
			$filePath = Environment::getPublicPath() . '/' .
				rtrim($this->storage->getConfiguration()['basePath'], '/') . $file['identifier'];
			if (!is_file($filePath) || is_file($filePath . '.webp')) {
				continue;
			}

			$imageString = file_get_contents($filePath);
			if ($imageString === '') {
				$this->log('File "' . $filePath . '" could not be converted to webp! Maybe the image is empty.');
				continue;
			}
			$image = imagecreatefromstring($imageString);
			$webpPath = $filePath . '.webp';
			if (!is_resource($image)) {
				$this->log('File "' . $filePath . '" could not be converted to webp! Maybe the file is not an image.');
				continue;
			}

			if (!imageistruecolor($image)) {
				imagepalettetotruecolor($image);
			}

			$webpImage = imagewebp($image, $webpPath, $imageQuality);
			// Also check for is_file because imagewebp could return true in spite of the process failing at creating
			// a webp image, see https://www.php.net/manual/de/function.imagewebp.php
			if (!$webpImage || !is_file($webpPath)) {
				$this->log('File "' . $filePath . '" could not be converted to webp! Converting failed!');
				imagedestroy($image);
				continue;
			}

			$originalFileSize = filesize($filePath);
			$webpFileSize = filesize($webpPath);
			$fileSizeSaving = 100 - $webpFileSize / $originalFileSize * 100;
			if ($fileSizeSaving < $minimumSavingPercentage) {
				//				$this->log(
				//					'The saving of converting the file "' . $webpPath . '"(' .
				//					$webpFileSize . ' bytes) is less then ' . $minimumSavingPercentage .
				//					'% of the original source file (' . $originalFileSize . ' bytes)!'
				//				);
				unlink($webpPath);
			}

			imagedestroy($image);
		}

		$start += $limit;
		$this->registry->set('sg_seo_webp', 'offset', $start);
		if ($start > $fileCount) {
			$this->registry->set('sg_seo_webp', 'offset', 0);
			return;
		}

		$this->workBatch($start, $limit, $fileCount);
	}

	/**
	 * Log an error message to sys_log in a readable format for the Log Module
	 *
	 * @param string $message
	 */
	protected function log(string $message): void {
		$useLog = (bool) $this->arguments['useLog'];
		if (!$useLog) {
			return;
		}

		$userId = 0;
		$workspace = 0;
		$data = [];
		/** @noinspection ProperNullCoalescingOperatorUsageInspection */
		$backendUser = $GLOBALS['BE_USER'] ?? new BackendUserAuthentication();
		if ($backendUser instanceof BackendUserAuthentication) {
			if (isset($backendUser->user['uid'])) {
				$userId = $backendUser->user['uid'];
			}
			if (isset($backendUser->workspace)) {
				$workspace = $backendUser->workspace;
			}
			if (!empty($backendUser->user['ses_backuserid'])) {
				$data['originalUser'] = $backendUser->user['ses_backuserid'];
			}
		}

		$connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('sys_log');
		$connection->insert(
			'sys_log',
			[
				'userid' => $userId,
				'type' => SystemLogType::ERROR,
				'action' => SystemLogGenericAction::UNDEFINED,
				'error' => SystemLogErrorClassification::MESSAGE,
				'details_nr' => 0,
				'details' => $message,
				'log_data' => empty($data) ? '' : serialize($data),
				'IP' => (string) GeneralUtility::getIndpEnv('REMOTE_ADDR'),
				'tstamp' => $GLOBALS['EXEC_TIME'],
				'workspace' => $workspace
			]
		);
	}
}
