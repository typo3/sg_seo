<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgSeo\HrefLang\EventListener;

use Doctrine\DBAL\Driver\Exception;
use SGalinski\SgSeo\Service\UrlGenerationService;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use TYPO3\CMS\Core\Context\LanguageAspectFactory;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Domain\Repository\PageRepository;
use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Event\ModifyHrefLangTagsEvent;
use TYPO3\Languagevisibility\Exception\TableNotSupportedException;
use TYPO3\Languagevisibility\Service\FrontendServices;

/**
 * Hreflang Generator
 */
class ModifyHrefLangListener {
	protected Context $context;
	protected SiteFinder $siteFinder;
	protected UrlGenerationService $urlGenerationService;
	protected ConnectionPool $connectionPool;
	protected PageRepository $pageRepository;

	public function __construct(
		Context $context,
		SiteFinder $siteFinder,
		ConnectionPool $connectionPool,
		PageRepository $pageRepository,
		UrlGenerationService $urlGenerationService
	) {
		$this->context = $context;
		$this->siteFinder = $siteFinder;
		$this->connectionPool = $connectionPool;
		$this->pageRepository = $pageRepository;
		$this->urlGenerationService = $urlGenerationService;
	}

	/**
	 * @throws SiteNotFoundException
	 * @throws Exception
	 * @throws AspectNotFoundException
	 */
	public function __invoke(ModifyHrefLangTagsEvent $event): void {
		// Only execute the first call to the hreflang generator. Otherwise, you will get multiple entries in the
		// output because the result is directly rendered to the header data. So the first call wins if multiple
		// plugins call the event caller.
		$GLOBALS['SgSeoHreflangsGenerated'] = (bool) ($GLOBALS['SgSeoHreflangsGenerated'] ?? FALSE);
		if ($GLOBALS['SgSeoHreflangsGenerated']) {
			$event->setHrefLangs([]);
			return;
		}

		$languageAspect = $this->context->getAspect('language');
		$currentLanguageId = $languageAspect->getId();
		$site = $this->siteFinder->getSiteByPageId($GLOBALS['TSFE']->id);
		$languages = $site->getLanguages();
		$isFirstLanguage = TRUE;
		$isLanguageVisibilityLoaded = ExtensionManagementUtility::isLoaded('languagevisibility');
		$newsLoaded = ExtensionManagementUtility::isLoaded('news');
		$hrefLangs = [];

		$newsId = 0;
		$newsAvailabilityChecker = NULL;
		if ($newsLoaded) {
			// Beware: Class might not be available in your system!
			$newsAvailabilityChecker = GeneralUtility::makeInstance('GeorgRinger\News\Seo\NewsAvailability');
			if ($newsAvailabilityChecker && class_exists('GeorgRinger\News\Seo\NewsAvailability')) {
				$newsId = $newsAvailabilityChecker->getNewsIdFromRequest();
			}
		}

		if (count($languages) > 1) {
			foreach ($languages as $language) {
				$hreflang = trim($language->getHreflang());
				if ($hreflang === '') {
					continue;
				}

				$languageId = $language->getLanguageId();
				if ($isLanguageVisibilityLoaded) {
					$frontendServices = GeneralUtility::makeInstance(FrontendServices::class);
					$isVisible = $frontendServices->checkVisiblityForElement(
						$GLOBALS['TSFE']->page,
						'pages',
						$languageId
					);
					if (!$isVisible) {
						continue;
					}
				}

				if ($newsLoaded && $newsId && class_exists('GeorgRinger\News\Seo\NewsAvailability')) {
					$isVisible = TRUE;
					try {
						// first try the news extension availability checker (only strict element checks)
						$isVisible = $newsAvailabilityChecker->check($languageId, $newsId);
						if ($isVisible) {
							// Secondly, let's try to fetch the news record regularly to have a proper languagevisibility check on
							$news = $this->getNews($languageId, $newsId);
							$isVisible = (bool) $news;
						}
					} catch (\Exception $e) {
						// intended
					}

					if (!$isVisible) {
						continue;
					}
				}

				$page = $GLOBALS['TSFE']->page;
				if ($languageId !== $currentLanguageId) {
					if ($languageId > 0) {
						// Check if a localization exists
						$languageAspect = LanguageAspectFactory::createFromSiteLanguage($language);
						$localization = $this->pageRepository->getLanguageOverlay('pages', $page, $languageAspect);
						if (is_array($localization)) {
							$page = $localization;
						}
					}

					// don't re-add "canonicalbyextension" as it's not multilingual safe
					// extensionArgumentsForCanonicalAndHrefLang must always be taken from the extension if missing
					if (
						!isset($page['extensionArgumentsForCanonicalAndHrefLang']) &&
						isset($GLOBALS['TSFE']->page['extensionArgumentsForCanonicalAndHrefLang'])
					) {
						$page['extensionArgumentsForCanonicalAndHrefLang'] =
							$GLOBALS['TSFE']->page['extensionArgumentsForCanonicalAndHrefLang'];
					}
				}

				$canonical = $this->urlGenerationService->generateUrlByCurrentRequest(
					$event->getRequest(),
					$languageId,
					TRUE,
					$page
				);
				if ($canonical === '') {
					continue;
				}

				// Duplicate URLs are ok inside the hreflang
				// and don't harm: https://www.searchviu.com/en/multiple-hreflang-tags-one-url/
				if ($isFirstLanguage) {
					$hrefLangs['x-default'] = $canonical;
				}
				$isFirstLanguage = FALSE;

				$hrefLangs[$hreflang] = $canonical;
			}
		}

		$GLOBALS['SgSeoHreflangsGenerated'] = TRUE;
		$event->setHrefLangs($hrefLangs);
	}

	/**
	 * Get specific news article
	 *
	 * @param int $sysLanguageUid
	 * @param int $newsId
	 * @return array
	 * @throws TableNotSupportedException
	 * @throws \Doctrine\DBAL\Exception
	 * @throws AspectNotFoundException
	 */
	private function getNews(int $sysLanguageUid, int $newsId): array {
		$isLanguageVisibilityLoaded = ExtensionManagementUtility::isLoaded('languagevisibility');
		if ($isLanguageVisibilityLoaded) {
			$frontendServices = GeneralUtility::makeInstance(FrontendServices::class);
		}

		$queryBuilder = $this->connectionPool->getQueryBuilderForTable('tx_news_domain_model_news');

		$conditions = [
			$queryBuilder->expr()->eq('uid', $newsId),
			$queryBuilder->expr()->in('sys_language_uid', '-1,0'),
			$queryBuilder->expr()->notIn('pid', '-1'),
			$queryBuilder->expr()->and(
				$queryBuilder->expr()->or(
					$queryBuilder->expr()->neq('type', '3'),
					$queryBuilder->expr()->and(
						$queryBuilder->expr()->eq('type', '3'),
						$queryBuilder->expr()->eq('reference_more', '1')
					)
				)
			),
		];

		$databaseResource = $queryBuilder->select('*')
			->from('tx_news_domain_model_news')->andWhere(...$conditions)->executeQuery();

		$visibleRows = [];
		$rows = $databaseResource->fetchAllAssociative();
		$context = GeneralUtility::makeInstance(Context::class);
		foreach ($rows as $row) {
			$table = 'tx_news_domain_model_news';
			$languageId = $row['sys_language_uid'];
			if ($isLanguageVisibilityLoaded) {
				// get languagevisibility uid that is available (check for the correct uid to fall back to)
				$element = $frontendServices->getElement($row, $table);
				$languageId = $frontendServices->getOverlayLanguageIdForElement(
					$element,
					$sysLanguageUid
				);
				if ($languageId === FALSE || $languageId === NULL) {
					continue;
				}
			}

			if ($languageId > 0) {
				/** @noinspection PhpPossiblePolymorphicInvocationInspection */
				$languageAspect = $context->getAspect('language');
				$row = $this->pageRepository->getLanguageOverlay($table, $row, $languageAspect);
			}

			$visibleRows[] = $row;
		}

		return $visibleRows;
	}
}
