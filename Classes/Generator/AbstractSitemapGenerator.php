<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgSeo\Generator;

use SGalinski\SgSeo\Events\OverrideSitemapRendererEvent;
use SGalinski\SgSeo\Renderers\AbstractSitemapRenderer;
use SGalinski\SgSeo\Renderers\StandardSitemapRenderer;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use TYPO3\CMS\Core\Context\LanguageAspectFactory;
use TYPO3\CMS\Core\Domain\Repository\PageRepository;
use TYPO3\CMS\Core\EventDispatcher\EventDispatcher;
use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\Site\Entity\Site;
use TYPO3\CMS\Core\Site\Entity\SiteLanguage;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * This class is a base for all sitemap generators.
 */
abstract class AbstractSitemapGenerator {
	/**
	 * @var AbstractSitemapRenderer
	 */
	protected $renderer;

	/**
	 * @var PageRepository
	 */
	protected $pageRepository;

	/**
	 * when true, leaves out no_index and hidden pages
	 *
	 * @var bool
	 */
	protected $enableFilter = TRUE;

	/**
	 * @var Context
	 */
	protected $context;

	/**
	 * @var SiteLanguage
	 */
	protected $language;

	/**
	 * @var int
	 */
	protected $pageId = 0;

	/**
	 * @var Site
	 */
	protected $site;

	/**
	 * @var bool
	 */
	protected $isLanguageVisibilityExtensionActive = FALSE;

	/**
	 * Hold a backup for each stupid aspect we change
	 *
	 * @var array
	 */
	public $backupAspects = [];

	/**
	 * @param boolean $enableFilter
	 * @return void
	 */
	public function setEnableFilter(bool $enableFilter): void {
		$this->enableFilter = $enableFilter;
	}

	/**
	 * Renderer setter
	 *
	 * @param AbstractSitemapRenderer $renderer
	 * @return void
	 */
	public function setRenderer(AbstractSitemapRenderer $renderer): void {
		$this->renderer = $renderer;
	}

	/**
	 * Initializes the instance of this class. This constructor sets starting
	 * point for the sitemap to the current page id
	 *
	 * @param int $pageId
	 * @param SiteLanguage|null $language
	 * @param bool $enableFilter
	 * @param string $xslPath
	 * @throws SiteNotFoundException
	 */
	public function __construct(
		int $pageId = 0,
		SiteLanguage $language = NULL,
		bool $enableFilter = TRUE,
		string $xslPath = ''
	) {
		$this->renderer = GeneralUtility::makeInstance(StandardSitemapRenderer::class);
		$overrideSitemapRenderEvent = GeneralUtility::makeInstance(OverrideSitemapRendererEvent::class);
		GeneralUtility::makeInstance(EventDispatcher::class)->dispatch($overrideSitemapRenderEvent);
		$newRenderer = $overrideSitemapRenderEvent->getRenderer();
		if ($newRenderer instanceof AbstractSitemapRenderer) {
			$this->renderer = $newRenderer;
		}

		$this->isLanguageVisibilityExtensionActive = ExtensionManagementUtility::isLoaded('languagevisibility');
		$this->enableFilter = $enableFilter;
		$this->pageId = $pageId;
		$this->renderer->setXslPath($xslPath);
		$this->site = GeneralUtility::makeInstance(SiteFinder::class)->getSiteByPageId($this->pageId);
		$this->context = GeneralUtility::makeInstance(Context::class);
		if ($language === NULL) {
			$language = $this->site->getDefaultLanguage();
		}

		$this->language = $language;
		$this->setupPageRepository();
	}

	/**
	 * writes back old values for changed aspects
	 *
	 * @return void
	 */
	public function takeDownPageRepository(): void {
		foreach ($this->backupAspects as $key => $value) {
			$this->context->setAspect($key, $value);
		}
	}

	/**
	 * sets Aspect with a Backup, use instead of $this->context->setAspect
	 *
	 * @param $name
	 * @param $value
	 * @return void
	 * @throws AspectNotFoundException
	 */
	public function setAspect($name, $value) {
		$this->backupAspects[$name] = $this->context->getAspect($name);
		$this->context->setAspect($name, $value);
	}

	/**
	 * Setup of the page repository
	 *
	 * BEWARE: The context is a Singleton!
	 */
	protected function setupPageRepository(): void {
		$pageRepository = PageRepository::class;

		$languageAspect = LanguageAspectFactory::createFromSiteLanguage($this->language);
		$this->setAspect('language', $languageAspect);
		$this->pageRepository = GeneralUtility::makeInstance($pageRepository, $this->context);
	}

	/**
	 * Writes sitemap content to the output.
	 *
	 * @param string $sitemapName
	 * @param array $sitemapEntries
	 * @return void
	 */
	public function main(string $sitemapName, array $sitemapEntries): void {
		header('Content-type: text/xml');
		echo $this->renderer->getStartTags();
		echo $this->generateSitemapContent($sitemapName, $sitemapEntries);
		echo $this->renderer->getEndTags();
	}

	/**
	 * Returns the sitemap content
	 *
	 * @param string $sitemapName
	 * @param array $sitemapEntries
	 * @return string
	 */
	public function returnSitemapContent(string $sitemapName, array $sitemapEntries): string {
		$content = $this->renderer->getStartTags();
		$content .= $this->generateSitemapContent($sitemapName, $sitemapEntries);
		$content .= $this->renderer->getEndTags();

		return $content;
	}

	/**
	 * Generates the sitemap
	 *
	 * @param string $sitemapName
	 * @param array $sitemapEntries
	 * @return string
	 */
	abstract protected function generateSitemapContent(string $sitemapName, array $sitemapEntries): string;

	/**
	 * Gets the entries of the sitemap
	 *
	 * @param string $sitemapName
	 * @return array
	 */
	abstract public function getSitemapEntries(string $sitemapName): array;
}
