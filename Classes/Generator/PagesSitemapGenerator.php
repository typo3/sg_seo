<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgSeo\Generator;

use Closure;
use Exception;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\NullLogger;
use SGalinski\SgSeo\Events\AccessPageListEvent;
use SGalinski\SgSeo\Events\ShouldIncludePageInSitemapEvent;
use TYPO3\CMS\Core\Authentication\Mfa\MfaRequiredException;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use TYPO3\CMS\Core\Context\LanguageAspectFactory;
use TYPO3\CMS\Core\Context\UserAspect;
use TYPO3\CMS\Core\Context\VisibilityAspect;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Core\SystemEnvironmentBuilder;
use TYPO3\CMS\Core\Domain\Repository\PageRepository;
use TYPO3\CMS\Core\EventDispatcher\EventDispatcher;
use TYPO3\CMS\Core\Http\ImmediateResponseException;
use TYPO3\CMS\Core\Http\ServerRequestFactory;
use TYPO3\CMS\Core\Localization\Locales;
use TYPO3\CMS\Core\Routing\PageArguments;
use TYPO3\CMS\Core\Routing\RouteNotFoundException;
use TYPO3\CMS\Core\Routing\SiteMatcher;
use TYPO3\CMS\Core\Site\Entity\Site;
use TYPO3\CMS\Core\Site\Entity\SiteLanguage;
use TYPO3\CMS\Core\Type\Bitmask\PageTranslationVisibility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Frontend\Authentication\FrontendUserAuthentication;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;
use TYPO3\CMS\Seo\XmlSitemap\XmlSitemapDataProviderInterface;
use TYPO3\Languagevisibility\Service\FrontendServices;

/**
 * This class produces sitemap for pages
 */
class PagesSitemapGenerator extends AbstractSitemapGenerator {
	public const DEFAULT_SITEMAP_INDEX = 'xmlSitemap';
	public const MAX_ENTRIES_PER_SITEMAP = 50000;

	/**
	 * @var array
	 */
	protected $excludedPageTypes = [
		PageRepository::DOKTYPE_LINK,
		PageRepository::DOKTYPE_SHORTCUT,
		PageRepository::DOKTYPE_BE_USER_SECTION,
		PageRepository::DOKTYPE_MOUNTPOINT,
		PageRepository::DOKTYPE_SPACER,
		PageRepository::DOKTYPE_SYSFOLDER,
		PageRepository::DOKTYPE_RECYCLER
	];

	/**
	 * @var PageRepository
	 */
	protected $pageRepositoryForDefaultLanguage;

	/**
	 * @var ServerRequestInterface
	 */
	protected $request;

	/**
	 * @var FrontendUserAuthentication
	 */
	protected $frontendUser;

	/**
	 * @var array
	 */
	protected $typoScriptConfiguration;

	/**
	 * The count of entries in the current sitemap
	 *
	 * @var int
	 */
	protected $currentSitemapCount = 0;

	/**
	 * Setup of the page repository
	 *
	 * BEWARE: The context is a Singleton!
	 */
	protected function setupPageRepository(): void {
		$pageRepository = PageRepository::class;

		// Let's save a special page repository for the default language for later usage
		// This needs to be called before the parent call as in this function, the context language is overridden again
		// for the main page repository. The language of the singleton is directly saved in the constructor of the
		// page repository. This is the whole reason why this works!
		$allLanguages = $this->site->getAllLanguages();
		$languageAspect = LanguageAspectFactory::createFromSiteLanguage($allLanguages[0]);

		$this->setAspect('language', $languageAspect);
		$this->pageRepositoryForDefaultLanguage = GeneralUtility::makeInstance($pageRepository, $this->context);

		parent::setupPageRepository();

		// exclude hidden pages in non fallback type modes
		// In fallback mode, this would exclude also pages if hidden only in the translation.
		// This is an error as they would fallback to default with still the correct URL.
		$isNonFallbackType = in_array($this->language->getFallbackType(), ['strict', 'free']);
		$aspect = new VisibilityAspect(!($isNonFallbackType && $this->language->getLanguageId() > 0));
		$this->setAspect('visibility', $aspect);
	}

	/**
	 * Returns the first page (root page or enforced site) to the page list
	 *
	 * @return array
	 */
	protected function getFirstPageOfPageList(): array {
		$page = $this->pageRepository->getPage($this->pageId);

		return [
			'uid' => $page['uid'],
			'SYS_LASTCHANGED' => $page['SYS_LASTCHANGED'],
			'tx_sgseo_lastmod' => $page['tx_sgseo_lastmod'],
			'sitemap_priority' => $page['sitemap_priority'],
			'sitemap_changefreq' => $page['sitemap_changefreq'],
			'doktype' => $page['doktype']
		];
	}

	/**
	 * Generates sitemap for pages (<url> entries in the sitemap)
	 *
	 * @param string $sitemapName
	 * @param array $sitemapEntries
	 * @return string
	 */
	protected function generateSitemapContent(string $sitemapName, array $sitemapEntries): string {
		$content = '';

		foreach ($sitemapEntries as $sitemapEntry) {
			$content .= $this->renderer->renderEntry(
				$sitemapEntry['loc'],
				$sitemapEntry['lastMod'],
				(string) ($sitemapEntry['changefreq'] ?? 'monthly'),
				($sitemapEntry['priority'] ?: 0.5)
			);
		}

		return $content;
	}

	/**
	 * @param string $sitemapName
	 * @return array
	 * @throws ImmediateResponseException
	 * @throws Exception|\Doctrine\DBAL\Driver\Exception
	 */
	public function getSitemapEntries(string $sitemapName): array {
		// fresh the setup page repository with the current language
		$this->setupPageRepository();

		// Skip default pages generation for non-default sitemaps
		if ($sitemapName !== self::DEFAULT_SITEMAP_INDEX) {
			return $this->generateAdditionalPages($sitemapName);
		}

		$entries = [];
		$pageList = [$this->getFirstPageOfPageList()];
		while (count($pageList)) {
			$pageInfo = array_shift($pageList);
			if ($this->shouldIncludePageInSitemap($pageInfo)) {
				$url = $this->getPageLink((int) $pageInfo['uid']);
				if ($url !== '') {
					$entries[] = [
						'loc' => $url,
						'lastMod' => $this->getLastModifiedDate($pageInfo),
						'changefreq' => $this->getChangeFrequency($pageInfo),
						'priority' => ($pageInfo['sitemap_priority'] ?: 0.5),
					];
				}
			}

			// add the subpages
			$subPages = $this->pageRepository->getMenu((int) $pageInfo['uid'], '*', '', '', FALSE);
			/** @noinspection SlowArrayOperationsInLoopInspection */
			$pageList = array_merge($pageList, array_values($subPages));
			unset($subPages, $pageInfo);
		}

		// provide a signal slot to manipulate the pageList
		$pageList = [];

		$accessPageListEvent = new AccessPageListEvent(
			$pageList,
			$this->language->getLanguageId(),
			$this->site,
			$this->getTypoScriptConfiguration()
		);
		GeneralUtility::makeInstance(EventDispatcher::class)->dispatch($accessPageListEvent);
		$pageList = $accessPageListEvent->getPageList();

		// write urls added by signal slot
		while (count($pageList)) {
			$pageInfo = array_shift($pageList);
			$entries[] = [
				'loc' => $pageInfo['url'],
				'lastMod' => $this->getLastModifiedDate($pageInfo),
				'changefreq' => $this->getChangeFrequency($pageInfo),
				'priority' => (isset($pageInfo['sitemap_priority']) && $pageInfo['sitemap_priority'] ?: 0.5)
			];
		}

		return array_merge($entries, array_values($this->generateAdditionalPages($sitemapName)));
	}

	/**
	 * Generates the output for the additional data provider content
	 *
	 * @param string $sitemapName
	 * @return array
	 * @throws AspectNotFoundException
	 * @throws MfaRequiredException
	 * @throws RouteNotFoundException
	 */
	protected function generateAdditionalPages(string $sitemapName): array {
		// Add additional pages from the extra data providers configured in the tx_seo TypoScript config
		$this->mockTSFE($this->context, $this->site, $this->language);
		return $this->getAdditionalSitemapPages($sitemapName);
	}

	/**
	 * Create and overwrite a TSFE object for this language
	 *
	 * @param Context $context
	 * @param Site $site
	 * @param SiteLanguage $language
	 * @throws AspectNotFoundException
	 * @throws MfaRequiredException
	 * @throws RouteNotFoundException
	 */
	protected function mockTSFE(Context $context, Site $site, SiteLanguage $language): void {
		unset($GLOBALS['TSFE']);
		$serverRequestFactory = new ServerRequestFactory();
		$backupRequest = $GLOBALS['TYPO3_REQUEST'] ?? NULL;
		$GLOBALS['TYPO3_REQUEST'] = $serverRequestFactory->createServerRequest('GET', $language->getBase());
		$GLOBALS['TYPO3_REQUEST'] = $GLOBALS['TYPO3_REQUEST']->withAttribute(
			'applicationType',
			SystemEnvironmentBuilder::REQUESTTYPE_FE
		);
		$siteMatcher = GeneralUtility::makeInstance(SiteMatcher::class);
		$matchResult = $siteMatcher->matchRequest($GLOBALS['TYPO3_REQUEST']);
		$pageRouter = $site->getRouter($context);
		/** @var PageArguments $pageArguments */
		$pageArguments = $pageRouter->matchRequest($GLOBALS['TYPO3_REQUEST'], $matchResult);
		$this->getFrontendUser($GLOBALS['TYPO3_REQUEST']);

		$GLOBALS['TSFE'] = $typoScriptFrontendController =
			new TypoScriptFrontendController($context, $site, $language, $pageArguments, $this->frontendUser);

		$typoScriptFrontendController->setLogger(new NullLogger());
		$typoScriptFrontendController->determineId($GLOBALS['TYPO3_REQUEST']);
		Locales::setSystemLocaleFromSiteLanguage(
			$typoScriptFrontendController->getLanguage()
		);
		$typoScriptFrontendController->linkVars = '&L=' . $language->getLanguageId();
		$typoScriptFrontendController->newCObj();

		$GLOBALS['TYPO3_REQUEST'] = $backupRequest;
	}

	/**
	 * @param $request
	 * @return void
	 * @throws MfaRequiredException
	 * @throws AspectNotFoundException
	 */
	public function getFrontendUser($request) {
		if (!$this->frontendUser) {
			$frontendUser = GeneralUtility::makeInstance(FrontendUserAuthentication::class);
			if (!Environment::isCli()) {
				// Authenticate now
				$frontendUser->start($request);
			}

			$this->frontendUser = $frontendUser;
		}

		$this->setAspect(
			'frontend.user',
			GeneralUtility::makeInstance(UserAspect::class, $this->frontendUser)
		);
	}

	/**
	 * Creates a new FrontendUserAuthentication object and injects it in the current context
	 *
	 * @throws AspectNotFoundException
	 * @throws MfaRequiredException
	 */
	public function mockFrontendUser(): void {
		if (!$this->frontendUser) {
			$frontendUser = GeneralUtility::makeInstance(FrontendUserAuthentication::class);
			if (!Environment::isCli()) {
				// Authenticate now
				$frontendUser->start($GLOBALS['TYPO3_REQUEST']);
			}

			$this->frontendUser = $frontendUser;
		}

		$this->setAspect(
			'frontend.user',
			GeneralUtility::makeInstance(UserAspect::class, $this->frontendUser)
		);

		if ($GLOBALS['TSFE'] && !is_object($GLOBALS['TSFE']->fe_user)) {
			$GLOBALS['TSFE']->fe_user = $this->frontendUser;
		}
	}

	/**
	 * Get the additional sitemap pages from other custom extensions sitemap data providers
	 *
	 * @param string $sitemapName
	 * @return array|mixed
	 */
	protected function getAdditionalSitemapPages(string $sitemapName) {
		$configuration = $this->getTypoScriptConfiguration()['plugin']['tx_seo'];

		$dataProviders = $this->getSitemapDataProviders($configuration, $sitemapName);
		$additionalPages = [];
		foreach ($dataProviders as $sitemap => $dataProvider) {
			// We don't use the EXT:seo data providers for the default sitemap, because we have covered this logic
			// in our own classes. Those pages are already in the sitemap.
			if ($sitemapName === self::DEFAULT_SITEMAP_INDEX && $sitemap === 'pages') {
				continue;
			}

			$ref = $this;
			foreach ($dataProvider['providers'] as $provider) {
				$closure = function () use ($provider) {
					$provider->numberOfItemsPerPage = 999999999;
					return $provider->getItems();
				};

				$itemsGetter = Closure::bind($closure, $provider, $provider);
				$items = $itemsGetter();

				$additionalPages += $items;
			}
		}

		foreach ($additionalPages as $key => $additionalPage) {
			$additionalPages[$key] = [
				'loc' => $additionalPage['loc'],
				'priority' => $additionalPage['priority'],
				'lastMod' => $additionalPage['lastMod'],
			];
		}

		return $additionalPages;
	}

	/**
	 * Returns a mocked ServerRequest, needed for the DataProviders to get their pages.
	 * Don't ask why - bad TYPO3 design.
	 *
	 * @return ServerRequestInterface
	 */
	protected function getRequest(): ServerRequestInterface {
		if (!$this->request) {
			$serverRequestFactory = GeneralUtility::makeInstance(ServerRequestFactory::class);
			$request = $serverRequestFactory->createServerRequest(
				'GET',
				$this->site->getRouter($this->context)->generateUri(
					$this->pageId,
					['_language' => $this->language->getLanguageId()]
				),
				['site' => $this->site]
			)->withAttribute('site', $this->site);

			$this->request = $request;
		}

		return $this->request;
	}

	/**
	 * Set the internal TypoScriptConfiguration
	 *
	 * @param array $typoScriptConfiguration
	 */
	public function setTypoScriptConfiguration(array $typoScriptConfiguration): void {
		$this->typoScriptConfiguration = $typoScriptConfiguration;
	}

	/**
	 * Returns the typo script configuration
	 *
	 * @return array
	 */
	public function getTypoScriptConfiguration(): array {
		return $this->typoScriptConfiguration;
	}

	/**
	 * Get the data providers for the current sitemap
	 *
	 * @param array $configuration
	 * @param string $sitemapName
	 * @return array
	 */
	protected function getSitemapDataProviders(array $configuration, string $sitemapName): array {
		$sitemaps = [];
		foreach ($configuration['config'][$sitemapName]['sitemaps'] ?? [] as $sitemap => $config) {
			if (
				class_exists($config['provider'])
				&& is_subclass_of($config['provider'], XmlSitemapDataProviderInterface::class)
			) {
				$sitemaps[$sitemap]['providers'][] = GeneralUtility::makeInstance(
					$config['provider'],
					$this->getRequest(),
					$sitemap,
					(array) $config['config']
				);
			}
		}

		return $sitemaps;
	}

	/**
	 * Obtains the last modification date of the page.
	 *
	 * @param array $pageInfo
	 * @return int
	 */
	protected function getLastModifiedDate(array $pageInfo): int {
		$lastModifiedDates = isset($pageInfo['tx_sgseo_lastmod']) ? GeneralUtility::intExplode(
			',',
			$pageInfo['tx_sgseo_lastmod']
		) : [];
		if (isset($pageInfo['SYS_LASTCHANGED'])) {
			$lastModifiedDates[] = (int) $pageInfo['SYS_LASTCHANGED'];
		}

		rsort($lastModifiedDates, SORT_NUMERIC);

		return current($lastModifiedDates);
	}

	/**
	 * Checks if the page should be included into the sitemap.
	 *
	 * It's intended that not all checks can be disabled by the global "enableFilter" setting
	 *
	 * @param array $pageInfo
	 * @return bool
	 * @throws Exception|\Doctrine\DBAL\Driver\Exception
	 */
	protected function shouldIncludePageInSitemap(array $pageInfo): bool {
		$languageId = $this->language->getLanguageId();
		if ($this->enableFilter) {
			// if the fallback type is requested, we need to check the hidden state of the DEFAULT record
			$isNonFallbackType = in_array($this->language->getFallbackType(), ['strict', 'free']);
			if (!$isNonFallbackType) {
				$originalPage = $this->pageRepositoryForDefaultLanguage->getPage($pageInfo['uid']);
				if (isset($originalPage['hidden']) && $originalPage['hidden']) {
					return FALSE;
				}
			} elseif (isset($pageInfo['hidden']) && $pageInfo['hidden']) {
				return FALSE;
			}

			// if filtering is enabled and noindex is set, exclude this page
			if (isset($pageInfo['no_index']) && (int) $pageInfo['no_index'] === 1) {
				return FALSE;
			}

			// if this page has another url in the canonical tag, skip it
			// Hint: The canonical tag highly probably references to another existing page (duplicate content!).
			if (
				isset($pageInfo['canonical_link'])
				&& (
					filter_var($pageInfo['canonical_link'], FILTER_VALIDATE_URL)
					|| filter_var('https://' . $pageInfo['canonical_link'], FILTER_VALIDATE_URL)
				)
			) {
				return FALSE;
			}
		}

		if (isset($pageInfo['doktype']) && in_array($pageInfo['doktype'], $this->excludedPageTypes, TRUE)) {
			return FALSE;
		}

		// Check l18n_cfg setting
		$pageTranslationVisibility = new PageTranslationVisibility($pageInfo['l18n_cfg'] ?? 0);
		$pageTranslationVisibility->shouldHideTranslationIfNoTranslatedRecordExists();
		if (
			$languageId === 0
			&& isset($pageInfo['l18n_cfg'])
			&& $pageTranslationVisibility->shouldHideTranslationIfNoTranslatedRecordExists()
		) {
			return FALSE;
		}

		if (
			$languageId !== 0
			&& !isset($pageInfo['_PAGES_OVERLAY'])
			&& isset($pageInfo['l18n_cfg'])
			&& $pageTranslationVisibility->shouldHideTranslationIfNoTranslatedRecordExists()
		) {
			return FALSE;
		}

		// check language_visibility
		if ($this->isLanguageVisibilityExtensionActive) {
			$frontendServices = GeneralUtility::makeInstance(FrontendServices::class);
			if (
				$languageId >= 0
				&& !$frontendServices->checkVisiblityForElement($pageInfo, 'pages', $languageId)
			) {
				return FALSE;
			}
		}

		// This Event is TRUE by default
		$shouldIncludeEvent = GeneralUtility::makeInstance(
			ShouldIncludePageInSitemapEvent::class,
			$pageInfo,
			$languageId
		);
		GeneralUtility::makeInstance(EventDispatcher::class)->dispatch($shouldIncludeEvent);
		return $shouldIncludeEvent->getPageValid();
	}

	/**
	 * Fetches change frequency value.
	 *
	 * @param array $pageInfo
	 * @return string
	 */
	protected function getChangeFrequency(array $pageInfo): string {
		if (isset($pageInfo['sitemap_changefreq']) && $pageInfo['sitemap_changefreq']) {
			$changeFrequency = $pageInfo['sitemap_changefreq'];
		} else {
			$changeFrequency = $this->calculateChangeFrequency($pageInfo);
		}

		return $changeFrequency;
	}

	/**
	 * Calculates change frequency.
	 *
	 * @param array $pageInfo
	 * @return string
	 */
	protected function calculateChangeFrequency(array $pageInfo): string {
		$timeValues = isset($pageInfo['tx_sgseo_lastmod']) ? GeneralUtility::intExplode(
			',',
			$pageInfo['tx_sgseo_lastmod']
		) : [];

		// Remove zeros
		foreach ($timeValues as $key => $value) {
			if ($value === 0) {
				unset($timeValues[$key]);
			}
		}

		if (isset($pageInfo['SYS_LASTCHANGED'])) {
			$timeValues[] = $pageInfo['SYS_LASTCHANGED'];
		}

		$timeValues[] = time();
		sort($timeValues, SORT_NUMERIC);
		$sum = 0;
		for ($i = count($timeValues) - 1; $i > 0; $i--) {
			$sum += ($timeValues[$i] - $timeValues[$i - 1]);
		}

		$average = ($sum / (count($timeValues) - 1));

		/** @noinspection NestedTernaryOperatorInspection */
		return
			($average >= 180 * 24 * 60 * 60 ? 'monthly' :
				($average <= 24 * 60 * 60 ? 'daily' :
					($average <= 60 * 60 ? 'hourly' :
						($average <= 14 * 24 * 60 * 60 ? 'weekly' : 'monthly'))));
	}

	/**
	 * Creates a link to a single page
	 *
	 * @param int $pageId
	 * @return string Full URL of the page including host name (escaped)
	 */
	protected function getPageLink(int $pageId): string {
		return htmlspecialchars(
			$this->site->getRouter($this->context)->generateUri(
				$pageId,
				['_language' => $this->language->getLanguageId()]
			)
		);
	}

	/**
	 * Gets the XSL File Path from the TypoScript configuration
	 *
	 * @param array $configuration
	 * @param string|null $sitemapType
	 * @param string|null $sitemap
	 * @return string
	 */
	public static function getXslFilePath(
		array $configuration,
		string $sitemapType = NULL,
		string $sitemap = NULL
	): string {
		$path = $configuration['config']['xslFile'] ?? 'EXT:seo/Resources/Public/CSS/Sitemap.xsl';
		$path = ($sitemapType !== NULL) ? ($configuration['config'][$sitemapType]['sitemaps']['xslFile'] ?? $path) : $path;
		$path = ($sitemapType !== NULL && $sitemap !== NULL) ? ($configuration['config'][$sitemapType]['sitemaps'][$sitemap]['config']['xslFile'] ?? $path) : $path;
		$path = PathUtility::getAbsoluteWebPath(GeneralUtility::getFileAbsFileName($path));
		if (!preg_match('/^(http|ftp|\/)/i', $path)) {
			$path = '/' . $path;
		}

		return $path;
	}
}
