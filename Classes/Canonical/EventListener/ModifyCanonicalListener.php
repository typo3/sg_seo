<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgSeo\Canonical\EventListener;

use Psr\Http\Message\ServerRequestInterface;
use SGalinski\SgSeo\Service\UrlGenerationService;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Seo\Event\ModifyUrlForCanonicalTagEvent;

/**
 * Canonical implementation
 */
class ModifyCanonicalListener {
	protected Context $context;
	protected UrlGenerationService $urlGenerationService;
	protected ServerRequestInterface $request;
	protected PageRenderer $pageRenderer;
	public function __construct(
		Context $context,
		UrlGenerationService $urlGenerationService,
		PageRenderer $pageRenderer
	) {
		$this->context = $context;
		$this->urlGenerationService = $urlGenerationService;
		$this->request = $GLOBALS['TYPO3_REQUEST'];
		$this->pageRenderer = $pageRenderer;
	}

	/**
	 * Renders the canonical based on different situations
	 *
	 * @throws SiteNotFoundException
	 * @throws AspectNotFoundException
	 */
	public function __invoke(ModifyUrlForCanonicalTagEvent $event): void {
		$controller = $GLOBALS['TSFE'];
		// first clear any canonical parts that are available
		foreach ($controller->additionalHeaderData as $key => $headerData) {
			if (str_contains($headerData, '<link rel="canonical"')) {
				unset($controller->additionalHeaderData[$key]);
			}
		}

		$languageId = $this->context->getPropertyFromAspect('language', 'id');
		$canonical = $this->urlGenerationService->generateUrlByCurrentRequest($this->request, $languageId);
		$event->setUrl($canonical);
	}
}
