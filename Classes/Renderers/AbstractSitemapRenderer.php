<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgSeo\Renderers;

/**
 * This class contains an abstract renderer for sitemaps.
 */
abstract class AbstractSitemapRenderer {
	/**
	 * @var string The path to the XSL stylesheet
	 */
	protected $xslPath;

	/**
	 * Creates start XML tags (including XML prologue) for the sitemap.
	 *
	 * @return string Start tags
	 */
	abstract public function getStartTags(): string;

	/**
	 * Renders one single entry according to the format of this sitemap.
	 *
	 * @param string $url URL of the entry
	 * @param int $lastModification News publication time (Unix timestamp)
	 * @param string $changeFrequency Unused for news
	 * @param mixed $priority Priority (numeric, 1-10, if passed)
	 * @return string Generated entry content
	 */
	abstract public function renderEntry(
		string $url,
		int $lastModification = 0,
		string $changeFrequency = '',
		$priority = ''
	): string;

	/**
	 * Creates end XML tags for this sitemap.
	 *
	 * @return string End XML tags
	 */
	abstract public function getEndTags(): string;

	/**
	 * @param string $xslPath
	 */
	public function setXslPath(string $xslPath): void {
		$this->xslPath = $xslPath;
	}
}
