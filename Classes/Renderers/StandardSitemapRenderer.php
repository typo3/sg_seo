<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgSeo\Renderers;

/**
 * This class contains a renderer for the 'normal' (not 'news') sitemap.
 */
class StandardSitemapRenderer extends AbstractSitemapRenderer {
	/**
	 * Creates end tags for this sitemap.
	 *
	 * @return string
	 */
	public function getEndTags(): string {
		return '</urlset>';
	}

	/**
	 * Creates start tags for this sitemap.
	 *
	 * @return string
	 */
	public function getStartTags(): string {
		return '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="' . $this->xslPath . '"?>'
			. chr(10) . '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . chr(10);
	}

	/**
	 * Renders a single entry as a normal sitemap entry.
	 *
	 * @param string $url URL of the entry
	 * @param int $lastModification News publication time (Unix timestamp)
	 * @param string $changeFrequency Unused for news
	 * @param mixed $priority Priority (numeric, 0.1-1, if passed)
	 * @return string Generated entry content
	 */
	public function renderEntry(
		string $url,
		int $lastModification = 0,
		string $changeFrequency = '',
		$priority = ''
	): string {
		$content = '<url>';
		$content .= '<loc>' . htmlentities($url) . '</loc>';
		$content .= '<lastmod>' . date('c', $lastModification) . '</lastmod>';
		$content .= '<changefreq>' . ($changeFrequency ?: 'monthly') . '</changefreq>';
		$content .= '<priority>' . sprintf('%0.1F', $priority) . '</priority>';
		$content .= '</url>';

		return $content;
	}
}
