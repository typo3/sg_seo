<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgSeo\Hooks;

use InvalidArgumentException;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;

/**
 * Page and content manipulation watch.
 */
class TceMain {
	/**
	 * If > 1 than we are in the recursive call to ourselves and we do not do anything
	 *
	 * @var    int
	 */
	protected $lock = 0;

	/**
	 * Maximum number of timestamps to save
	 *
	 */
	public const MAX_ENTRIES = 5;

	/** @var int[] */
	protected static $recordedPages = [];

	/**
	 * Hooks to data change procedure to watch modified data. This hook is called
	 * after data is written to the database, so all paths are modified paths.
	 *
	 * @param string $status Record status (new or update)
	 * @param string $table Table name
	 * @param int $id Record ID
	 * @param array $fieldArray Modified fields
	 * @param DataHandler $pObj Reference to TCEmain
	 * @throws InvalidArgumentException
	 */
	public function processDatamap_afterDatabaseOperations(
		/** @noinspection PhpUnusedParameterInspection */
		$status,
		$table,
		$id,
		array $fieldArray,
		DataHandler $pObj
	) {
		// Only for LIVE records!
		if ((int) $pObj->BE_USER->workspace === 0 && !$this->lock) {
			$this->lock++;
			$this->recordPageChange($table, $id, $fieldArray, $pObj);
			$this->lock--;
		}
	}

	/**
	 * Records page change time in our own field
	 *
	 * @param string $table Table name
	 * @param int $id ID of the record
	 * @param array $fieldArray Field array
	 * @param DataHandler $pObj Reference to TCEmain
	 * @return    void
	 * @throws InvalidArgumentException
	 */
	protected function recordPageChange(
		$table,
		$id,
		array $fieldArray,
		DataHandler $pObj
	) {
		if (is_numeric($id) && ($pid = $this->getPid(
			$table,
			$id,
			$fieldArray,
			$pObj
		)) && !isset(self::$recordedPages[$pid])) {
			self::$recordedPages[$pid] = 1;

			$record = BackendUtility::getRecord('pages', $pid, 'tx_sgseo_lastmod');
			$elements = $record['tx_sgseo_lastmod'] === '' ? [] : GeneralUtility::trimExplode(
				',',
				$record['tx_sgseo_lastmod']
			);
			$time = time();
			// We must check if this time stamp is already in the list. This
			// happens with many independent updates of the page during a
			// single TCEmain action
			if (!\in_array($time, $elements, TRUE)) {
				$elements[] = $time;
				if (\count($elements) > self::MAX_ENTRIES) {
					$elements = \array_slice($elements, -self::MAX_ENTRIES);
				}

				$datamap = [
					'pages' => [
						$pid => [
							'tx_sgseo_lastmod' => implode(',', $elements),
						],
					],
				];
				$tce = GeneralUtility::makeInstance(DataHandler::class);
				/* @var DataHandler $tce */
				$tce->start($datamap, []);
				$tce->enableLogging = FALSE;
				$tce->updateDB('pages', $pid, $datamap['pages'][$pid]);
			}
		}
	}

	/**
	 * Obtains page id from the arguments
	 *
	 * @param string $table Table name
	 * @param int $id ID of the record
	 * @param array $fieldArray Field array
	 * @param DataHandler $pObj Reference to TCEmain
	 * @return int
	 */
	protected function getPid($table, $id, array $fieldArray, DataHandler $pObj): int {
		if (!MathUtility::canBeInterpretedAsInteger($id)) {
			$id = $pObj->substNEWwithIDs[$id];
		}
		if ($table !== 'pages') {
			if (isset($fieldArray['pid']) && $fieldArray['pid'] >= 0 && MathUtility::canBeInterpretedAsInteger(
				$fieldArray['pid']
			)) {
				$id = $fieldArray['pid'];
			} else {
				$record = BackendUtility::getRecord($table, $id, 'pid');
				$id = $record['pid'];
			}
		}
		return (int) $id;
	}
}
