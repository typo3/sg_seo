<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgSeo\Updates;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;
use TYPO3\CMS\Install\Updates\ChattyInterface;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

/**
 * Rename SEO Columns
 *
 * @author Paul Ilea <paul@sgalinski.de>
 */
#[UpgradeWizard('tx_sgseo_migrate_seo_fields_to_typo3')]
class MigrateSEOFieldsToTYPO3 implements UpgradeWizardInterface, ChattyInterface {
	/**
	 * @var OutputInterface
	 */
	public $output;

	/**
	 * Setter injection for output into upgrade wizards
	 *
	 * @param OutputInterface $output
	 */
	public function setOutput(OutputInterface $output): void {
		$this->output = $output;
	}

	/**
	 * Return the speaking name of this wizard
	 *
	 * @return string
	 */
	public function getTitle(): string {
		return 'Migrate SG SEO fields to EXT:seo';
	}

	/**
	 * Return the description for this wizard
	 *
	 * @return string
	 */
	public function getDescription(): string {
		return 'Migrate SG SEO fields to EXT:seo which is now a dependency.';
	}

	/**
	 * Execute the update
	 *
	 * Called when a wizard reports that an update is necessary
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function executeUpdate(): bool {
		$connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('pages');
		$query = 'UPDATE pages
		SET sitemap_changefreq = IFNULL(SUBSTRING(tx_sgseo_change_frequency, 1, 10), \'\'),
		sitemap_priority = tx_sgseo_priority/10,
		og_image = media';
		$connection->executeQuery($query);

		$this->migrateSocialImages($connection);

		return TRUE;
	}

	/**
	 * Migrate the media files to og_image and twitter_image
	 *
	 * @param Connection $connection
	 * @throws \Doctrine\DBAL\Driver\Exception
	 * @throws Exception
	 */
	protected function migrateSocialImages(Connection $connection) {
		$getPagesQuery = "SELECT uid FROM pages WHERE media = 1";
		$parentPages = $connection->executeQuery($getPagesQuery);
		while ($parentPage = $parentPages->fetchOne()) {
			// Get old file reference
			$fileReferenceQuery = "SELECT *
				FROM sys_file_reference
				WHERE tablenames = 'pages' AND fieldname = 'media'
					AND uid_foreign = $parentPage";
			$fileReferences = $connection->executeQuery($fileReferenceQuery);
			while ($oldFileReference = $fileReferences->fetch()) {
				$newFileReference = $oldFileReference;
				unset($newFileReference['uid']);
				$newFileReference['crdate'] = $newFileReference['tstamp'] = $GLOBALS['EXEC_TIME'];
				$newFileReference['fieldname'] = 'og_image';
				$connection->insert('sys_file_reference', $newFileReference);
				$newFileReference['fieldname'] = 'twitter_image';
				$connection->insert('sys_file_reference', $newFileReference);
			}
		}
	}

	/**
	 * Is an update necessary?
	 *
	 * Is used to determine whether a wizard needs to be run.
	 * Check if data for migration exists.
	 *
	 * @return bool
	 */
	public function updateNecessary(): bool {
		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('pages');
		$pageFields = $queryBuilder->getConnection()->getSchemaManager()->listTableColumns('pages');
		return isset($pageFields['tx_sgseo_change_frequency']);
	}

	/**
	 * Returns an array of class names of Prerequisite classes
	 *
	 * This way a wizard can define dependencies like "database up-to-date" or
	 * "reference index updated"
	 *
	 * @return string[]
	 */
	public function getPrerequisites(): array {
		return [];
	}
}
