<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgSeo\Updates;

use Doctrine\DBAL\Driver\Exception;
use InvalidArgumentException;
use SGalinski\SgNews\Domain\Model\Category;
use SGalinski\SgNews\Domain\Model\News;
use SGalinski\SgNews\Domain\Repository\CategoryRepository;
use SGalinski\SgNews\Domain\Repository\NewsRepository;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

/**
 * Migrate template db entries to the correct root pages
 */
#[UpgradeWizard('tx_sgseo_migrate_sgnews_teaser_image_to_og_image')]
class MigrateNewsTeaserImageToFacebookAndTwitterImage implements UpgradeWizardInterface {
	/**
	 * @return string
	 */
	public function getTitle(): string {
		return 'Migrate sg_news teaser images to og_image and twitter_image.';
	}

	/**
	 * @return string
	 */
	public function getDescription(): string {
		return 'Create new author entries for the old author field of news pages';
	}

	/**
  * @return bool
  * @throws InvalidQueryException|Exception
  */
	public function executeUpdate(): bool {
		$newsRepository = GeneralUtility::makeInstance(NewsRepository::class);
		$categoryRepository = GeneralUtility::makeInstance(CategoryRepository::class);
		$query = $newsRepository->createQuery();
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$newsEntries = $query->execute();

		$newsMetaData = [];
		$categories = [];
		foreach ($newsEntries as $newsEntry) {
			/** @var News $newsEntry */
			/** @var Category $category */
			$categoryUid = $newsEntry->getPid();
			$category = $categories[$categoryUid] ?? FALSE;
			if (!$category) {
				$category = $categoryRepository->findByUid($categoryUid);
				if (!$category) {
					continue;
				}
				$categories[$categoryUid] = $category;
			}

			$metaData = $this->getMetaDataForNews($newsEntry, $category);

			if (!$metaData['teaserImage']) {
				continue;
			}

			$newsMetaData[$newsEntry->getUid()] = $metaData;

			if ($newsEntry->_getProperty('_languageUid') !== 0) {
				$newsMetaData[$newsEntry->getUid()]['languageData'] = $this->getLanguageData($newsEntry);
			} else {
				$newsMetaData[$newsEntry->getUid()]['languageData'] = [
					'uid' => $newsEntry->getUid(),
					'l10n_parent' => 0,
					'sys_language_uid' => 0
				];
			}
		}

		$this->saveTwitterAndOgImageFromTeasers($newsMetaData);

		return TRUE;
	}

	/**
	 * Gets the translation related data for the news page
	 *
	 * @param News $news
	 * @return array|false|mixed
	 * @throws Exception
	 */
	protected function getLanguageData(News $news) {
		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
			->getQueryBuilderForTable('pages');
		$queryBuilder->getRestrictions()->removeAll();
		return $queryBuilder->select('uid', 'l10n_parent', 'sys_language_uid')
			->from('pages')->where($queryBuilder->expr()->eq('uid', $news->getUid()))->executeQuery()->fetch();
	}

	/**
	 * Creates new sys_file_references for og_image and twitter_image
	 *
	 * @param array $newsMetaData
	 */
	protected function saveTwitterAndOgImageFromTeasers($newsMetaData): void {
		$connection = GeneralUtility::makeInstance(ConnectionPool::class)
			->getConnectionForTable('pages');
		foreach ($newsMetaData as $pageId => $newsMetaDatum) {
			$fileObject = $newsMetaDatum['teaserImageObject']->getOriginalResource()->getOriginalFile();
			// Assemble DataHandler data
			$connection->update('pages', ['og_image' => 1], ['uid' => $pageId]);
			$connection->insert('sys_file_reference', [
				'table_local' => 'sys_file',
				'uid_local' => $fileObject->getUid(),
				'tablenames' => 'pages',
				'uid_foreign' => $pageId,
				'fieldname' => 'og_image',
				'pid' => ($newsMetaDatum['languageData']['sys_language_uid'] === 0)
					? $pageId : $newsMetaDatum['languageData']['l10n_parent'],
				'sys_language_uid' => $newsMetaDatum['languageData']['sys_language_uid'],
				'l10n_parent' => 0,
				'sorting_foreign' => 1,
				'crdate' => $GLOBALS['EXEC_TIME'],
				'tstamp' => $GLOBALS['EXEC_TIME'],
			]);

			$connection->update('pages', ['twitter_image' => 1], ['uid' => $pageId]);
			$connection->insert('sys_file_reference', [
				'table_local' => 'sys_file',
				'uid_local' => $fileObject->getUid(),
				'tablenames' => 'pages',
				'uid_foreign' => $pageId,
				'fieldname' => 'twitter_image',
				'pid' => ($newsMetaDatum['languageData']['sys_language_uid'] === 0)
					? $pageId : $newsMetaDatum['languageData']['l10n_parent'],
				'sys_language_uid' => $newsMetaDatum['languageData']['sys_language_uid'],
				'l10n_parent' => 0,
				'sorting_foreign' => 1,
				'crdate' => $GLOBALS['EXEC_TIME'],
				'tstamp' => $GLOBALS['EXEC_TIME'],
			]);
		}
	}

	/**
	 * @return bool
	 */
	public function updateNecessary(): bool {
		return ExtensionManagementUtility::isLoaded('sg_news');
	}

	/**
	 * @return array|string[]
	 */
	public function getPrerequisites(): array {
		return [];
	}

	/**
	 * Returns the metadata of the given news.
	 *
	 * @param News $news
	 * @param Category $category
	 * @return array
	 * @throws InvalidArgumentException
	 */
	protected function getMetaDataForNews(News $news, Category $category): array {
		$newsId = $news->getUid();
		if (isset($this->cachedSingleNews[$newsId])) {
			return $this->cachedSingleNews[$newsId];
		}

		$singleNewsImageData = $this->getDataForSingleViewImage($news, $category);
		$teaserImageData = $this->getDataForTeaserImage($news, $category);

		// Use single news image data for teaser image data, if the teaser imaga data are empty.
		$teaserIsEmpty = $teaserImageData['teaserImage'] === NULL && $teaserImageData['teaserImageObject'] === NULL;
		$singleNewsIsEmpty = $singleNewsImageData['image'] === NULL && $singleNewsImageData['imageObject'] === NULL;
		if ($teaserIsEmpty && !$singleNewsIsEmpty) {
			$teaserImageData = [
				'teaserImage' => $singleNewsImageData['image'],
				'teaserImageObject' => $singleNewsImageData['imageObject'],
			];
		}

		return array_merge(
			[
				'category' => $category,
				'news' => $news,
			],
			$singleNewsImageData,
			$teaserImageData
		);
	}

	/**
	 * Returns the single view image data as an array for the given news and category as fallback.
	 *
	 * @param News $news
	 * @param Category $category
	 * @return array
	 * @throws InvalidArgumentException
	 */
	protected function getDataForSingleViewImage(News $news, Category $category): array {
		/** @var FileReference $singleNewsImage */
		$singleNewsImage = $singleNewsImageObject = NULL;
		$singleNewsImages = $news->getTeaser2Image();
		if (count($singleNewsImages)) {
			$singleNewsImage = $singleNewsImages->current();
		} else {
			$categoryImages = $category->getTeaser2Image();
			if (count($categoryImages)) {
				$singleNewsImage = $categoryImages->current();
			}
		}

		if ($singleNewsImage) {
			$singleNewsImageObject = $singleNewsImage;
			$originalResource = $singleNewsImage->getOriginalResource();
			if ($originalResource) {
				$singleNewsImage = $originalResource->getPublicUrl();
			}

			if ($singleNewsImage) {
				$singleNewsImage = GeneralUtility::getIndpEnv('TYPO3_SITE_PATH') . $singleNewsImage;
			}
		}

		return [
			'image' => $singleNewsImage,
			'imageObject' => $singleNewsImageObject,
		];
	}

	/**
	 * Returns the teaser image data as an array for the given news and category as fallback.
	 *
	 * @param News $news
	 * @param Category $category
	 * @return array
	 * @throws InvalidArgumentException
	 */
	protected function getDataForTeaserImage(News $news, Category $category): array {
		/** @var FileReference $teaserImage */
		$teaserImage = $teaserImageObject = NULL;
		$teaserImages = $news->getTeaser1Image();
		if (count($teaserImages)) {
			$teaserImage = $teaserImages->current();
		} else {
			$categoryImages = $category->getTeaser1Image();
			if (count($categoryImages)) {
				$teaserImage = $categoryImages->current();
			}
		}

		if ($teaserImage) {
			$teaserImageObject = $teaserImage;
			$originalResource = $teaserImage->getOriginalResource();
			if ($originalResource) {
				$teaserImage = $originalResource->getPublicUrl();
			}

			if ($teaserImage) {
				$teaserImage = GeneralUtility::getIndpEnv('TYPO3_SITE_PATH') . $teaserImage;
			}
		}

		return [
			'teaserImage' => $teaserImage,
			'teaserImageObject' => $teaserImageObject,
		];
	}
}
