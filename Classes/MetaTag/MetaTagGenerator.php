<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgSeo\MetaTag;

use Psr\EventDispatcher\EventDispatcherInterface;
use SGalinski\SgSeo\Events\OverrideOgImagesEvent;
use SGalinski\SgSeo\Events\OverrideTwitterImagesEvent;
use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\MetaTag\MetaTagManagerRegistry;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Service\ImageService;
use TYPO3\CMS\Frontend\Resource\FileCollector;

/**
 * Extends the TYPO3 MetaTagGenerator in order to introduce custom logic for some meta tag fields. We did not use
 * hooks or the built-in API, because they do not provide enough flexibility the way they have been implemented
 */
class MetaTagGenerator extends \TYPO3\CMS\Seo\MetaTag\MetaTagGenerator {
	private EventDispatcherInterface $eventDispatcher;

	public function __construct(
		MetaTagManagerRegistry $metaTagManagerRegistry,
		ImageService $imageService,
		EventDispatcherInterface $eventDispatcher
	) {
		parent::__construct($metaTagManagerRegistry, $imageService);
		$this->eventDispatcher = $eventDispatcher;
	}

	/**
	 * @inheritdoc
	 * @throws SiteNotFoundException
	 */
	public function generate(array $params): void {
		$metaTagManagerRegistry = GeneralUtility::makeInstance(MetaTagManagerRegistry::class);

		if (!empty($params['page']['description'])) {
			$manager = $metaTagManagerRegistry->getManagerForProperty('description');
			$manager->addProperty('description', $params['page']['description']);
		}

		if (!empty($params['page']['og_title'])) {
			$manager = $metaTagManagerRegistry->getManagerForProperty('og:title');
			$manager->addProperty('og:title', $params['page']['og_title']);
		}

		if (!empty($params['page']['og_description'])) {
			$manager = $metaTagManagerRegistry->getManagerForProperty('og:description');
			$manager->addProperty('og:description', $params['page']['og_description']);
		}

		$manager = $metaTagManagerRegistry->getManagerForProperty('twitter:card');
		$manager->addProperty('twitter:card', $params['page']['twitter_card'] ?: 'summary_large_image');

		if (!empty($params['page']['twitter_title'])) {
			$manager = $metaTagManagerRegistry->getManagerForProperty('twitter:title');
			$manager->addProperty('twitter:title', $params['page']['twitter_title']);
		}

		if (!empty($params['page']['twitter_description'])) {
			$manager = $metaTagManagerRegistry->getManagerForProperty('twitter:description');
			$manager->addProperty('twitter:description', $params['page']['twitter_description']);
		}

		$noIndex = ((bool) $params['page']['no_index']) ? 'noindex' : 'index';
		$noFollow = ((bool) $params['page']['no_follow']) ? 'nofollow' : 'follow';

		if ($noIndex === 'noindex' || $noFollow === 'nofollow') {
			$manager = $metaTagManagerRegistry->getManagerForProperty('robots');
			$manager->addProperty('robots', implode(',', [$noIndex, $noFollow]));
		}

		##########################################
		### BEGIN - sg_seo (order is important, because of the rootline inheritance!)
		###
		### - Use images provided by extensions for og:image and twitter:image
		### - Override the rootline to allow that values are inherited if not explicitly set on the current page
		### - Render some additional fields
		##########################################
		$rootLineFields = [];
		if (empty($GLOBALS['TSFE']->page['og_image_path_by_extension'])) {
			$rootLineFields[] = 'og_image';
		}

		if (empty($GLOBALS['TSFE']->page['twitter_image_path_by_extension'])) {
			$rootLineFields[] = 'twitter_image';
		}

		// Get values from rootline
		if ($rootLineFields) {
			$this->findAndReplaceRootLineValue($rootLineFields, $params);
		}

		$this->handleOgImage($params, $metaTagManagerRegistry);
		$this->handleTwitterImage($params, $metaTagManagerRegistry);

		// Get site-related configuration
		$pid = $GLOBALS['TSFE']->page['uid'];
		$siteFinder = GeneralUtility::makeInstance(SiteFinder::class);
		$site = $siteFinder->getSiteByPageId($pid);
		$configuration = $site->getConfiguration();

		if (!empty($configuration['favicon'])) {
			$GLOBALS['TSFE']->pageRenderer->setFavIcon($configuration['favicon']);
		}

		if (!empty($configuration['article_publisher'])) {
			$manager = $metaTagManagerRegistry->getManagerForProperty('article:publisher');
			$manager->addProperty('article:publisher', $configuration['article_publisher']);
		}

		if (!empty($configuration['twitter_site'])) {
			$manager = $metaTagManagerRegistry->getManagerForProperty('twitter:site');
			$manager->addProperty('twitter:site', $configuration['twitter_site']);
		}

		##########################################
		### END - sg_seo
		##########################################
	}

	/**
	 * Gets the proper og_image data and generates the meta tag
	 *
	 * @param array $params
	 * @param MetaTagManagerRegistry $metaTagManagerRegistry
	 */
	protected function handleOgImage(array $params, MetaTagManagerRegistry $metaTagManagerRegistry): void {
		$ogImages = [];
		if (!empty($GLOBALS['TSFE']->page['og_image_path_by_extension'])) {
			$ogImages = [$GLOBALS['TSFE']->page['og_image_path_by_extension']];
		} elseif (!empty($params['page']['og_image'])) {
			$fileCollector = GeneralUtility::makeInstance(FileCollector::class);
			$fileCollector->addFilesFromRelation('pages', 'og_image', $params['page']);
			$ogImages = $fileCollector->getFiles();
		}

		// dispatch event, so that other extensions can modify/override the og:images here
		/** @var OverrideOgImagesEvent $overrideOgImagesEvent */
		$overrideOgImagesEvent = $this->eventDispatcher->dispatch(
			new OverrideOgImagesEvent($ogImages)
		);
		$ogImages = $this->generateSocialImages($overrideOgImagesEvent->getOgImages());

		if (!empty($ogImages)) {
			$manager = $metaTagManagerRegistry->getManagerForProperty('og:image');

			foreach ($ogImages as $ogImage) {
				$subProperties = [];
				$subProperties['url'] = $ogImage['url'];
				$subProperties['width'] = $ogImage['width'];
				$subProperties['height'] = $ogImage['height'];

				if (!empty($ogImage['alternative'])) {
					$subProperties['alt'] = $ogImage['alternative'];
				}

				$manager->addProperty(
					'og:image',
					$ogImage['url'],
					$subProperties
				);
			}
		}
	}

	/**
	 * Gets the proper twitter_image data and generates the meta tag
	 *
	 * @param array $params
	 * @param MetaTagManagerRegistry $metaTagManagerRegistry
	 */
	protected function handleTwitterImage(array $params, MetaTagManagerRegistry $metaTagManagerRegistry): void {
		$twitterImages = [];
		if (!empty($GLOBALS['TSFE']->page['twitter_image_path_by_extension'])) {
			$twitterImages = [$GLOBALS['TSFE']->page['twitter_image_path_by_extension']];
		} elseif (!empty($params['page']['twitter_image'])) {
			$fileCollector = GeneralUtility::makeInstance(FileCollector::class);
			$fileCollector->addFilesFromRelation('pages', 'twitter_image', $params['page']);
			$twitterImages = $fileCollector->getFiles();
		}

		// dispatch event, so that other extensions can modify/override the twitter:images here
		/** @var OverrideTwitterImagesEvent $overrideTwitterImagesEvent */
		$overrideTwitterImagesEvent = $this->eventDispatcher->dispatch(
			new OverrideTwitterImagesEvent($twitterImages)
		);

		$twitterImages = $this->generateSocialImages($overrideTwitterImagesEvent->getTwitterImages());

		if (!empty($twitterImages)) {
			$manager = $metaTagManagerRegistry->getManagerForProperty('twitter:image');

			foreach ($twitterImages as $twitterImage) {
				$subProperties = [];

				if (!empty($twitterImage['alternative'])) {
					$subProperties['alt'] = $twitterImage['alternative'];
				}

				$manager->addProperty(
					'twitter:image',
					$twitterImage['url'],
					$subProperties
				);
			}
		}
	}


	/**
	 * Overwrite. Only Change: fileExtension->webp
	 *
	 * NOTE: Currently disabled, because not supported by all platforms.
	 *
	 * https://twitter.com/jdevalk/status/1544256590882242560
	 *
	 * @param FileReference $fileReference
	 * @return FileInterface
	 */
	//	protected function processSocialImage(FileReference $fileReference): FileInterface {
	//		$arguments = $fileReference->getProperties();
	//		$cropVariantCollection = CropVariantCollection::create((string) ($arguments['crop'] ?? ''));
	//		$cropVariantName = ($arguments['cropVariant'] ?? FALSE) ?: 'social';
	//		$cropArea = $cropVariantCollection->getCropArea($cropVariantName);
	//		$crop = $cropArea->makeAbsoluteBasedOnFile($fileReference);
	//
	//		$processingConfiguration = [
	//			'crop' => $crop,
	//			'maxWidth' => 2000,
	//			'fileExtension' => 'webp'
	//		];
	//
	//		// The image needs to be processed if:
	//		//  - the image width is greater than the defined maximum width, or
	//		//  - there is a cropping other than the full image (starts at 0,0 and has a width and height of 100%) defined
	//		$needsProcessing = $fileReference->getProperty('width') > $processingConfiguration['maxWidth']
	//			|| !$cropArea->isEmpty();
	//		if (!$needsProcessing) {
	//			return $fileReference->getOriginalFile();
	//		}
	//
	//		return $fileReference->getOriginalFile()->process(
	//			ProcessedFile::CONTEXT_IMAGECROPSCALEMASK,
	//			$processingConfiguration
	//		);
	//	}

	/**
	 * Find the rootline values and replace them in the params
	 *
	 * @param array $fields
	 * @param array $params
	 */
	public function findAndReplaceRootLineValue(array $fields, array &$params): void {
		$rootLineIndex = 0;
		$rootLine = $GLOBALS['TSFE']->tmpl->rootLine;
		$rootlineCount = is_countable($rootLine) ? count($rootLine) : 0;
		while ($rootLineIndex < $rootlineCount) {
			$rootLineEntry = $rootLine[$rootLineIndex];
			$entryFound = FALSE;
			foreach ($fields as $field) {
				$foundRootLineValue = $rootLineEntry[$field] ?? FALSE;
				if (!$foundRootLineValue) {
					continue;
				}

				$entryFound = TRUE;
				$params['page'][$field] = $foundRootLineValue;
			}

			if ($entryFound) {
				$params['page']['uid'] = $rootLineEntry['uid'];
				if (isset($params['page']['_LOCALIZED_UID'], $rootLineEntry['_LOCALIZED_UID'])) {
					$params['page']['_LOCALIZED_UID'] = $rootLineEntry['_LOCALIZED_UID'];
				}

				if (isset($params['page']['_PAGES_OVERLAY_UID'], $rootLineEntry['_PAGES_OVERLAY_UID'])) {
					$params['page']['_PAGES_OVERLAY_UID'] = $rootLineEntry['_PAGES_OVERLAY_UID'];
				}
			}

			++$rootLineIndex;
		}
	}
}
