<?php

$GLOBALS['SiteConfiguration']['site']['columns']['favicon'] = [
	'label' => 'LLL:EXT:sg_seo/Resources/Private/Language/locallang_db.xlf:siteconfiguration.favicon',
	'description' => 'LLL:EXT:sg_seo/Resources/Private/Language/locallang_db.xlf:siteconfiguration.favicon.description',
	'config' => [
		'type' => 'input',
	],
];

$GLOBALS['SiteConfiguration']['site']['columns']['article_publisher'] = [
	'label' => 'LLL:EXT:sg_seo/Resources/Private/Language/locallang_db.xlf:siteconfiguration.article_publisher',
	'description' => 'LLL:EXT:sg_seo/Resources/Private/Language/locallang_db.xlf:siteconfiguration.article_publisher.description',
	'config' => [
		'type' => 'input',
	],
];

$GLOBALS['SiteConfiguration']['site']['columns']['twitter_site'] = [
	'label' => 'LLL:EXT:sg_seo/Resources/Private/Language/locallang_db.xlf:siteconfiguration.twitter_site',
	'description' => 'LLL:EXT:sg_seo/Resources/Private/Language/locallang_db.xlf:siteconfiguration.twitter_site.description',
	'config' => [
		'type' => 'input',
	],
];

$GLOBALS['SiteConfiguration']['site']['types']['0']['showitem'] = str_replace(
	'base,',
	'base, favicon, article_publisher, twitter_site, ',
	$GLOBALS['SiteConfiguration']['site']['types']['0']['showitem']
);
