<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use SGalinski\SgSeo\Canonical\EventListener\ModifyCanonicalListener;
use SGalinski\SgSeo\Command\GenerateSitemapCommandController;
use SGalinski\SgSeo\Command\GenerateWebPCommand;
use SGalinski\SgSeo\HrefLang\EventListener\ModifyHrefLangListener;
use SGalinski\SgSeo\MetaTag\MetaTagGenerator;
use SGalinski\SgSeo\Service\HeadTagService;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use TYPO3\CMS\Frontend\Event\ModifyHrefLangTagsEvent;
use TYPO3\CMS\Seo\Event\ModifyUrlForCanonicalTagEvent;

return static function (ContainerConfigurator $containerConfigurator): void {
	$services = $containerConfigurator->services();
	$services->defaults()
		->private()
		->autowire()
		->autoconfigure();
	$services->load('SGalinski\\SgSeo\\', __DIR__ . '/../Classes/');
	$services->set(ModifyHrefLangListener::class)
		->tag('event.listener', [
			'identifier' => 'sg-seo/modifyHreflangGenerator',
			'after' => 'typo3-seo/hreflangGenerator',
			'event' => ModifyHrefLangTagsEvent::class
		]);
	$services->set(ModifyCanonicalListener::class)
		->tag('event.listener', [
			'identifier' => 'sg-seo/modifyCanonicalGenerator',
			'after' => 'legacy-slot',
			'event' => ModifyUrlForCanonicalTagEvent::class
		]);
	$services->set(HeadTagService::class)
		->args([
			'$isCachable' => 'TRUE',
			'$title' => '',
			'$description' => '',
			'$extensionArgumentsForCanonicalAndHrefLang' => ''
		]);
	$services->set(GenerateSitemapCommandController::class)
		->tag('console.command', ['command' => 'sg_seo:generateSitemap']);
	$services->set(GenerateWebPCommand::class)
		->tag('console.command', ['command' => 'sg_seo:generateWebP']);
	$services->set(MetaTagGenerator::class)
		->public();
};
