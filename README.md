# Ext: sg_seo

<img src="https://www.sgalinski.de/typo3conf/ext/project_theme/Resources/Public/Images/logo.svg" alt="" />

License: [GNU GPL, Version 2](https://www.gnu.org/licenses/gpl-2.0.html)

Repository: https://gitlab.sgalinski.de/typo3/sg_seo

Please report bugs here: https://gitlab.sgalinski.de/typo3/sg_seo

## About

This extension provides a Google sitemap implementation with additional features like:

- ignoring pages with canonical links to other pages
- respecting noindex & nofollow options
- ignoring shortcuts and status pages

## Scheduler

There are two scheduler tasks for this extension

1. A scheduler task to generate a sitemap for each language
2. A scheduler task to generate webp versions of the processed files

### Sitemap generation

This scheduler task can be activated to create static sitemaps of each language, which then can be delivered instead of
a dynamic process which could take a long time.

The task takes 3 arguments:

1. relativePathToSitemap: Describes where to save the generated sitemap files in the file system. This defaults to the
   project root.
2. siteRootId: The id of the site root, where the sitemap will start
3. enableFilter: Switch to add hidden pages (does not affect "NOT_IN_MENU") to the sitemap

### WebP generation

This scheduler task can generate webp versions of the images in processed folders. It uses the PHP builtin
function `imagewebp` and thus relies on the gd-extension to be installed (default requirement of TYPO3).

The task takes 5 arguments:

1. storageUid: The uid of the file storage to process. Defaults to 1 which usually is the fileadmin
2. savingPercentage: Define the minimum percentage of filesize savings the process has to achieve. By default, it is set
   to 10
3. imageQuality: Define the image quality. This is set to 80 by default.
4. batchSize: The number of files to process in one go (Note: The task will not stop after the amount of files, but
   fetch the amount of files, then process them before fetching the next batch. If it crashes, the task can be executed
   again, and it will continue with the batch it last worked on.) Defaults to 500.
5. processGif: Include gif images in the processing, defaults to 0, which means no

#### Apache .htaccess

```
<IfModule mod_rewrite.c>
    RewriteEngine On
    RewriteCond %{HTTP_ACCEPT} image/webp
    RewriteCond %{DOCUMENT_ROOT}/$1.$2.webp -f
    RewriteRule ^(fileadmin/.+)\.(png|jpe?g)$ $1.$2.webp [L]
</IfModule>

<IfModule mod_headers.c>
    Header append Vary Accept env=REDIRECT_accept
</IfModule>

AddType image/webp .webp
```

#### nginx

Add a map directive in your global nginx configuration:

```
map $http_accept $webp_suffix {
    default   "";
    "~*webp"  ".webp";
}
```

and add these rules to your server configuration:

```
location ~* ^/fileadmin/.+\.(png|jpg|jpeg)$ {
        add_header Vary Accept;
        try_files $uri$webp_suffix $uri =404;
}
location ~* ^/other-storage/.+\.(png|jpg|jpeg)$ {
        add_header Vary Accept;
        try_files $uri$webp_suffix $uri =404;
}
```

Make sure that there are no other rules that already apply to the specified image formats and prevent further execution!

## Extensions - SeoAccessPageListEventListener

You can hook into the process to extend the sitemap easily.

Example:
Services.yaml

```Yaml
services:
    MyExt\Event\Listener\SeoAccessPageListEventListener:
        tags:
            -   name: event.listener
                identifier: 'accessPageListEvent'
                event: SGalinski\SgSeo\Events\AccessPageListEvent
```

EventListener class:

```PHP
class SeoAccessPageListEventListener {
    public function __invoke(\SGalinski\SgSeo\Events\AccessPageListEvent $event) {
        $pageList = $event->getPageList();
        // ... do something with the list and write it back to the event
        $pageList[] = [
            'url' => 'MY FURTHER URL',
            'title' => ''
        ];
        $event->setPageList($pageList);
    }
}
```

## More Events

Additionally there are new Events with sg_seo 5.3:

OverrideSitemapRendererEvent

allows to set a different Renderer for the final output

ShouldIncludePageInSitemapEvent

given an array of $pageInfo and a $language, you can decide not to use a page in normal pageRendering

## Extensions - XmlSitemapDataProviders

We also support the official XmlSitemapDataProviders API of TYPO3. You can see more
here https://docs.typo3.org/m/typo3/reference-coreapi/master/en-us/ApiOverview/XmlSitemap/Index.html .

When we generate the sitemap, we generate every sitemap type to a new index. The default xmlSitemap type is being merged
into our sitemap.xml. Please bear in mind, that we ignore any additional data providers for the `pages` unique key, as
this is the default TYPO3 unique key and we don't want any duplicates. Use another unique key for your extension.

The other sitemap types are generated into their own respective sitemap indices.

Example (News extension):

```
plugin.tx_seo {
    config {
        xmlSitemap { // new entries to the default sitemap.xml
            sitemaps {
                news {
                    provider = GeorgRinger\News\Seo\NewsXmlSitemapDataProvider
                    config {
                        ...
                    }
                }
            }
        }
        googleNewsSitemap { // a completely new googlenewssitemap.xml
            sitemaps {
                news {
                    provider = GeorgRinger\News\Seo\NewsXmlSitemapDataProvider
                    config {
                        googleNews = 1
                        ...
                        template = GoogleNewsXmlSitemap.html
                    }
                }
            }
        }
    }
}
```

This example will result into the `news` unique key data provider entries being added into the default sitemap (
xmlSitemap).
The `googleNewsSitemap` entry will fetch the entries from this dataprovider and create a brand new sitemap index.

## Changing the values of your <head> tags like <title>, <description> etc.

Overrinding the values of <title> etc. is not trivial, as TYPO3's PageRenderer is sometimes called after caching the
HTML
output for output for the <head>. For this purpose we introduced the HeadTagService that you can call in your
Controllers.
It comes handy when you have plugins with database driven models and you want their metadata to be used for the page
metadata.
Check out this example with jobs:

```
$headTagService = GeneralUtility::makeInstance(
    HeadTagService::class,
    TRUE,
    $job->getTitle(),
    $job->getDescription(),
    '&tx_sgjobs_jobapplication[jobId]=' . $jobId
);
$headTagService->execute();
```

The 2nd parameter is very important - TRUE/FALSE describes whether the ControllerAction from which you are calling is
cachable or not. This way the service decides what is the proper way to override the values of the next arguments:

- title
- description
- GET arguments that must be appended to the router for the generation of the canonical and hreflang tags



