<?php

use SGalinski\SgSeo\Controller\GoogleSitemapController;
use SGalinski\SgSeo\Hooks\TceMain;
use SGalinski\SgSeo\MetaTag\MetaTagGenerator;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Extbase\Utility\ExtensionUtility;

call_user_func(
	static function () {
		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass']['sg_seo'] =
			TceMain::class;

		$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['sg_seo']['sitemap']['pages'] =
			'SGalinski\\SgSeo\\Generator\\PagesSitemapGenerator->main';

		$GLOBALS['TYPO3_CONF_VARS']['FE']['addRootLineFields'] .= ',og_image, twitter_image';

		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['TYPO3\CMS\Frontend\Page\PageGenerator']['generateMetaTags']['metatag'] =
			MetaTagGenerator::class . '->generate';
	}
);
