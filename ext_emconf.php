<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "sg_seo".
 ***************************************************************/

$EM_CONF['sg_seo'] = [
	'title' => 'Extended SEO Integration',
	'description' => 'Extended SEO Integration for TYPO3 based on EXT:seo',
	'category' => 'fe',
	'author' => 'Stefan Galinski',
	'author_email' => 'stefan@sgalinski.de',
	'state' => 'stable',
	'author_company' => 'sgalinski Internet Services (https://www.sgalinski.de)',
	'version' => '7.0.4',
	'constraints' => [
		'depends' =>
			[
				'typo3' => '12.4.0-12.4.99',
				'php' => '8.1.0-8.3.99'
			],
		'conflicts' =>
			[
			],
		'suggests' =>
			[
			],
	],
];
